<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('value');
        });

        DB::table('campaign')->insert([
            'key' => 'start_time',
            'value' => '2010-01-01T00:00:00+08:00',
        ]);

        DB::table('campaign')->insert([
            'key' => 'end_time',
            'value' => '2100-12-31T00:00:00+08:00',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
