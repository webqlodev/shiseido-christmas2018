<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'webqlo',
            'email' => 'dev@webqlo.com',
            'password' => bcrypt('webqlo!@#$'),
        ]);

        DB::table('users')->insert([
            'name' => 'foo',
            'email' => 'sb.foo@webqlo.com',
            'password' => bcrypt('webqlo123'),
        ]);

        DB::table('users')->insert([
            'name' => 'shiseido',
            'email' => 'no-reply@shiseidochristmas.com',
            'password' => bcrypt('shiseido!@#$'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
