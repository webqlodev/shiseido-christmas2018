<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\GiftList;

class CreateGiftList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_list', function($table)
        {
            $table->increments('id');
            $table->string('image_name');
            $table->string('image_title');
            $table->string('slug');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
        GiftList::create(
            ['image_name' => 'SMK F LASH VOL. MASCARA BK901','image_title' => '../img/products/full-lash-mascara.jpg',
            // 'image_description' => 'Full-Lash-Volume-Mascara',
            'slug' => 'Full-Lash-Volume-Mascara']
        );
        GiftList::create(
            ['image_name' => 'Essential Energy Day Emulsion (5ml)','image_title' => '../img/products/Essential-Energy Day-Emulsion-(5ml).png',
            // 'image_description' => 'Essential-Energy-Day-Emulsion-5ml',
            'slug' => 'Essential-Energy-Day-Emulsion-5ml']
        );
        GiftList::create(
            ['image_name' => 'Quick Makeup Voucher','image_title' => '../img/products/Quick-Makeup-voucher.png',
            // 'image_description' => 'Quick-Makeup-voucher',
            'slug' => 'Quick-Makeup-voucher']
        );
        GiftList::create(
            ['image_name' => '2018 EE TAGLA - BLUE','image_title' => '../img/products/SEE-Tagla-Blue-18.jpg',
            // 'image_description' => 'SEE-Tagla-Blue-18',
            'slug' => 'SEE-Tagla-Blue-18']
        );
        GiftList::create(
            ['image_name' => '2017 SMK FDN CARD.HLD(PEACOCK)','image_title' => '../img/products/smk-fdn-card-hld.jpg',
            // 'image_description' => 'smk-fdn-card-hld',
            'slug' => 'smk-fdn-card-hld']
        );
        GiftList::create(
            ['image_name' => "EVERBLOOM CLUTCH'16",'image_title' => '../img/products/Everbloom-Clutch.png',
            // 'image_description' => 'Everbloom-Clutch',
            'slug' => 'Everbloom-Clutch']
        );
        GiftList::create(
            ['image_name' => "BOW BRUSH SET",'image_title' => '../img/products/bow-brush-set.jpg',
            // 'image_description' => 'bow-brush-set',
            'slug' => 'bow-brush-set']
        );
        GiftList::create(
            ['image_name' => "SIB QUICK FIX MIST 13ML",'image_title' => '../img/products/Ibuki-quick-fix-mist.jpg',
            // 'image_description' => 'Ibuki-quick-fix-mist',
            'slug' => 'Ibuki-quick-fix-mist']
        );
        GiftList::create(
            ['image_name' => "UTM P IN EYE CONCENT SS 5ML",'image_title' => '../img/products/utm-eye5ml.jpg',
            // 'image_description' => 'utm-eye5ml',
            'slug' => 'utm-eye5ml']
        );
        GiftList::create(
            ['image_name' => "SMK TINTED GEL CREAM 2 SS 5ml",'image_title' => '../img/products/Tinted-Gel-Cream.jpg',
            // 'image_description' => 'Tinted-Gel-Cream',
            'slug' => 'Tinted-Gel-Cream']
        );
        GiftList::create(
            ['image_name' => "GSC PERF UV PROTECTOR SS 7ML",'image_title' => '../img/products/Perfect-UV-Protector-7ml.jpg',
            // 'image_description' => 'Perfect-UV-Protector-7ml',
            'slug' => 'Perfect-UV-Protector-7ml']
        );
        GiftList::create(
            ['image_name' => "BN WRK24 NIGHT CREAM 10ML",'image_title' => '../img/products/WrinkleResist24-Night-Cream-(10ml).png',
            // 'image_description' => 'WrinkleResist24-Night-Cream-10ml',
            'slug' => 'WrinkleResist24-Night-Cream-10ml']
        );
        GiftList::create(
            ['image_name' => "16SS CAMELLIA MIRROR",'image_title' => '../img/products/Camellia-Mirror.png',
            // 'image_description' => 'Camellia-Mirror',
            'slug' => 'Camellia-Mirror']
        );
        GiftList::create(
            ['image_name' => "SMK FULL LASH SERUM 2G",'image_title' => '../img/products/smk-full-lash-serum-travel.jpg',
            // 'image_description' => 'SMK-FULL-LASH-SERUM-2G',
            'slug' => 'SMK-FULL-LASH-SERUM-2G']
        );
        GiftList::create(
            ['image_name' => "SGS PURIFYING MASK 15ML",'image_title' => '../img/products/sts-purifying-mask-new.jpg',
            // 'image_description' => 'SGS-PURIFYING-MASK-15ML',
            'slug' => 'SGS-PURIFYING-MASK-15ML']
        );
        GiftList::create(
            ['image_name' => "TRANS LOOSE PWD REP (T/S) 2g",'image_title' => '../img/products/smk-loose-powder.jpg',
            // 'image_description' => 'TRANS-LOOSE-PWD-REP-2g',
            'slug' => 'TRANS-LOOSE-PWD-REP-2g']
        );
        GiftList::create(
            ['image_name' => "SBL BODY LOTION SS 30ML WO/C",'image_title' => '../img/products/sbl-everbloom-body-lotion-30ml.jpg',
            // 'image_description' => 'SBL-BODY-LOTION-SS-30ML-WO/C',
            'slug' => 'SBL-BODY-LOTION-SS-30ML-WO/C']
        );
        GiftList::create(
            ['image_name' => "2017 SMK FDN CARD.HLD (BIRDS)",'image_title' => '../img/products/2017-SMK-FDN-CARD-HLD-(BIRDS).jpg',
            // 'image_description' => '2017-SMK-FDN-CARD-HLD-BIRDS',
            'slug' => '2017-SMK-FDN-CARD-HLD-BIRDS']
        );
        GiftList::create(
            ['image_name' => "COSMETIC POUCH (BIG)",'image_title' => '../img/products/Cosmetic-Pouch.png',
            // 'image_description' => 'Cosmetic-Pouch',
            'slug' => 'Cosmetic-Pouch']
        );
        GiftList::create(
            ['image_name' => "UTM CLUTCH BAG",'image_title' => '../img/products/Ultimune-Clutch-Bag.png',
            // 'image_description' => 'Ultimune-Clutch-Bag',
            'slug' => 'Ultimune-Clutch-Bag']
        );
        GiftList::create(
            ['image_name' => "SMK CASE (C CP) RIBBONESIA VB",'image_title' => '../img/products/SMK-CASE-C-CP-RIBBONESIA-VB.jpg',
            // 'image_description' => 'SMK-CASE-C-CP-RIBBONESIA-VB',
            'slug' => 'SMK-CASE-C-CP-RIBBONESIA-VB']
        );
        GiftList::create(
            ['image_name' => "SMK CASE (C CP) RIBBONESIA VW",'image_title' => '../img/products/SMK-CASE-C-CP-RIBBONESIA-VW.jpg',
            // 'image_description' => 'SMK-CASE-C-CP-RIBBONESIA-VB',
            'slug' => 'SMK-CASE-C-CP-RIBBONESIA-VW']
        );
        GiftList::create(
            ['image_name' => "SMK ROUGE ROUGE RD124 AW17 x20pcs",'image_title' => '../img/products/smk-rouge.jpg',
            // 'image_description' => 'smk-rouge',
            'slug' => 'smk-rouge']
        );
        GiftList::create(
            ['image_name' => "SMK EXPRESSIVE DELUXE LIP OS",'image_title' => '../img/products/SMK-EXPRESSIVE-DELUXE-LIP-OS.jpg',
            // 'image_description' => 'SMK-EXPRESSIVE-DELUXE-LIP-OS',
            'slug' => 'SMK-EXPRESSIVE-DELUXE-LIP-OS']
        );
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gift_list');
    }
}
