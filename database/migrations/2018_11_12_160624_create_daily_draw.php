<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\DailyDraw;

class CreateDailyDraw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_draw', function($table)
        {
            $table->increments('id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('gift_id');
            $table->integer('quantity');
            $table->integer('day');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
        
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '1']
        );

        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '1']
        );

        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '4', 'quantity' => '5','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '8', 'quantity' => '5','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-01 00:00:00','end_date' => '2018-12-01 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '1']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '8', 'quantity' => '5','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-02 00:00:00','end_date' => '2018-12-02 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '2']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '4', 'quantity' => '5','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '8', 'quantity' => '5','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '11', 'quantity' => '6','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-03 00:00:00','end_date' => '2018-12-03 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '3']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-04 00:00:00','end_date' => '2018-12-04 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '4']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '4', 'quantity' => '5','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '8', 'quantity' => '5','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-05 00:00:00','end_date' => '2018-12-05 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '5']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-06 00:00:00','end_date' => '2018-12-06 23:59:59', 'gift_id' => '11', 'quantity' => '4','day' => '6']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '4', 'quantity' => '5','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-07 00:00:00','end_date' => '2018-12-07 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '7']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-08 00:00:00','end_date' => '2018-12-08 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '8']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-09 00:00:00','end_date' => '2018-12-09 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '9']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '5', 'quantity' => '13','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-10 00:00:00','end_date' => '2018-12-10 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '10']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '11', 'quantity' => '10','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-11 00:00:00','end_date' => '2018-12-11 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '11']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-12 00:00:00','end_date' => '2018-12-12 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '12']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-13 00:00:00','end_date' => '2018-12-13 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '13']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-14 00:00:00','end_date' => '2018-12-14 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '14']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '11', 'quantity' => '4','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-15 00:00:00','end_date' => '2018-12-15 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '15']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-16 00:00:00','end_date' => '2018-12-16 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '16']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '6', 'quantity' => '4','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '11', 'quantity' => '6','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-17 00:00:00','end_date' => '2018-12-17 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '17']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-18 00:00:00','end_date' => '2018-12-18 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '18']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '2', 'quantity' => '10','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '18', 'quantity' => '13','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '7', 'quantity' => '7','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '15', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '9', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-19 00:00:00','end_date' => '2018-12-19 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '19']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '12', 'quantity' => '25','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '3', 'quantity' => '10','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '20', 'quantity' => '8','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '13', 'quantity' => '5','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '1', 'quantity' => '2','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '18', 'quantity' => '15','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-20 00:00:00','end_date' => '2018-12-20 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '20']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '21', 'quantity' => '1','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '22', 'quantity' => '1','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '1', 'quantity' => '2','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '15', 'quantity' => '10','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '8', 'quantity' => '10','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '9', 'quantity' => '20','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '14', 'quantity' => '1','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-21 00:00:00','end_date' => '2018-12-21 23:59:59', 'gift_id' => '11', 'quantity' => '10','day' => '21']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '23', 'quantity' => '5','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '24', 'quantity' => '1','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '21', 'quantity' => '1','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '1', 'quantity' => '2','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '17', 'quantity' => '5','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '15', 'quantity' => '10','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '8', 'quantity' => '10','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '9', 'quantity' => '20','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '11', 'quantity' => '5','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-22 00:00:00','end_date' => '2018-12-22 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '22']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '23', 'quantity' => '5','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '24', 'quantity' => '1','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '22', 'quantity' => '1','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '1', 'quantity' => '2','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '17', 'quantity' => '10','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '15', 'quantity' => '10','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '8', 'quantity' => '20','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '10', 'quantity' => '10','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '11', 'quantity' => '10','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-23 00:00:00','end_date' => '2018-12-23 23:59:59', 'gift_id' => '16', 'quantity' => '5','day' => '23']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '23', 'quantity' => '5','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '24', 'quantity' => '1','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '21', 'quantity' => '1','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '22', 'quantity' => '1','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '1', 'quantity' => '2','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '17', 'quantity' => '10','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '15', 'quantity' => '10','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '8', 'quantity' => '20','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '9', 'quantity' => '10','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '11', 'quantity' => '10','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-24 00:00:00','end_date' => '2018-12-24 23:59:59', 'gift_id' => '16', 'quantity' => '10','day' => '24']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '24', 'quantity' => '2','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '23', 'quantity' => '5','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '21', 'quantity' => '2','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '22', 'quantity' => '2','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '1', 'quantity' => '10','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '17', 'quantity' => '10','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '15', 'quantity' => '10','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '19', 'quantity' => '5','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '8', 'quantity' => '20','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '11', 'quantity' => '10','day' => '25']
        );
        DailyDraw::create(
            ['start_date' => '2018-12-25 00:00:00','end_date' => '2018-12-25 23:59:59', 'gift_id' => '16', 'quantity' => '10','day' => '25']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_draw');
    }
}
