<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('main');
Route::post('/', 'HomeController@submit');
Route::get('/thank-you/{code}', 'HomeController@thankYou')->name('thank-you');
Route::get('/redirect/{location}', 'HomeController@redirect')->name('redirect');
Route::get('/end', function() { return view('end');});
/**admin**/
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('/save-date', 'AdminController@saveDate')->name('save-date');

Route::get('/resend-email/{code}', 'AdminController@resendEmail')->name('resend-email');
Route::get('/check-claim', 'AdminController@checkClaim');
Route::get('/export-registration-list', 'AdminController@exportRegistrationList')->name('export-registration-list');
Route::get('/export-notfilled-list', 'AdminController@exportNotfilledList')->name('export-notfilled-list');
Route::get('/export-fbshare-list', 'AdminController@exportFbshareList')->name('export-fbshare-list');

Route::get('/total-registration', 'AdminController@datatablesTotalRegistration')->name('total-registration');
Route::get('/total-sharing-record', 'AdminController@datatablesTotalSharingRecord')->name('total-sharing-record');
Route::get('/registration-list', 'AdminController@datatablesRegistrationList')->name('registration-list');
Route::get('/notfilled-list', 'AdminController@datatablesNotfilledList')->name('notfilled-list');
Route::get('/gift-list', 'AdminController@datatablesGiftList')->name('gift-list');

Route::get('/form','HomeController@new_form');
Route::get('/upload-excel','HomeController@upload_excel');
Route::get('/generate-email-code','HomeController@generate_email_code');
Route::get('/resend-form','HomeController@resend_form_email');
Route::post('/submit-new-form','HomeController@submit_new_form');
Route::get('auth/facebook', 'Auth\AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleFacebookProvider');
Route::post('/save-user', 'AjaxController@save_user');
Route::post('/insert-image','AjaxController@insert_image');
Route::post('/insert-product','AjaxController@insert_product');
Route::post('/submit-form','AjaxController@submit_form');
Route::post('/get-product','AjaxController@get_product');
Route::post('/check-status','AjaxController@check_status');
Route::get('/get-campaign-status','AjaxController@get_campaign_status');
Route::post('/get-expired-gift','AjaxController@get_expired_gift');
Route::post('/save-share-post','AjaxController@save_share_post');
Route::post('/find-gift-path','AjaxController@find_gift_path');

Route::get('/test-email', 'HomeController@test_email')->name('test-email');
// Route::get('/get-gift-details','AjaxController@get_gift_detail');

Auth::routes();
