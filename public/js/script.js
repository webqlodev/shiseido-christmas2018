$(function(){ //scroll to effect for all anchor link within same page.
	$('a').click(function(e){
		var href = $(this).attr('href');
		if(href.startsWith('#') && $(href).length > 0){
			$('html, body').animate({
                scrollTop: $(href).offset().top - 0
            }, 1000);
            closeMenu();
            e.preventDefault();
		}
	})
});

function toggleBannerVideoSound(){
	if( $(".banner-video").prop('muted') ) {
          $(".banner-video").prop('muted', false);
    } else {
      $(".banner-video").prop('muted', true);
    }
}

function closeMenu(){
	if($('.mobile-menu-icon').hasClass('close-menu')){
		$('.mobile-menu-icon').toggleClass("open-menu close-menu");
		$('.mobile-menu').slideToggle("show-menu");
	}
}
$(function(){
	$.snowfall.start({
	  size: {
	      min: 10,
	      max: 30
	  },
	  color: '#fff',
	  content: '&#8226;',
	  interval: 500,
  	  // disappear: 'linear'
	});
});
