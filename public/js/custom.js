function toggleImage(){
	$('.mobile-menu-icon').toggleClass("open-menu close-menu");
	$('.mobile-menu').slideToggle("show-menu");
}

function checkStatus(){
	if(localStorage.getItem("user") == "" || localStorage.getItem("user") == undefined || localStorage.getItem("user") == null){
		// $('#fbLoginModal').modal('show');
		// $('#fbLoginModal').css('padding-right', '0');
		
	}else{
		// checkResult();
		var fb_id = localStorage.getItem("user");
		$.ajax({
			type:'POST',
			url:'/check-status',
			data:{
				fb_id: fb_id
			},
			success: function(data){
				$.each(data, function(k,v){
					if(k == "early"){
						for(i = 1; i <= v; i++){
							$('#date-'+i).css("background-image","url(../img/calendar/"+i+".png)");
						}
					}
					else{
						for(i = 1; i <= v;i++){
							$('#date-'+i).css("background-image","url(../img/open_calendar/"+i+".png)");
							// $('#date-'+i).addClass("opened");
						}
					}
				});
		    }
		})
	}
	// var elmnt = document.getElementById("calendar-list");
 //    elmnt.scrollIntoView(true);
}
function checkCampaignStatus(){
	$.ajax({
		type:'GET',
		url:'/get-campaign-status',
		data:{
		},
		success: function(data){
			$.each(data, function(k,v){
				$.each(v, function(i,j){
					$('#date-'+j['day']).css("background-image","url(../img/open_calendar/"+j['day']+".png)");
					$('#date-'+j['day']).addClass('expired');
				})
			})
		}
	})
}
var calendar_click = 0;
var login_test = 0;
function triggerPopup(option){
	// $('#submitModal').modal('show');
	// $('#submitModal').css('top', '30%');
	calendar_click = option;
	// if((localStorage.getItem("user") == "" || localStorage.getItem("user") == undefined || localStorage.getItem("user") == null) && login_test<2){
	
	if((localStorage.getItem("user") == "" || localStorage.getItem("user") == undefined || localStorage.getItem("user") == null)){
		$('#fbLoginModal').modal('show');
		// login_test++;
	}else if( $('#date-'+option).hasClass("expired") && $('#date-'+option).hasClass("opened")){
		getExpiredGift(option);
	}else if($('#date-'+option).hasClass("opened")){
		$('#loseHolidayModal').modal('show');
		// $('#loseHolidayModal').css('padding-right', '0');
	}else{
		checkResult();
	}
	// var elmnt = document.getElementById("calendar-list");
    // elmnt.scrollIntoView({behavior: "instant", block: "center", inline: "center"});
}

function getExpiredGift(day){
	var fb_id = localStorage.getItem("user");
	// console.log(fb_id);
	$.ajax({
		type:'POST',
		url:'/get-expired-gift',
		data:{
			day: day,
			fb_id: fb_id
		},
		success: function(data){
			$.each(data, function(k,path){
				if(k == "claimed"){
					$('#happyHolidayModal').modal('show');
				}
				else if(k == "lose"){
					$('#thankYouModal').modal('show');
				}
				else if(k == "end"){
					$('#endModal').modal('show');
					$('#endModal .modal-body #date').html(path);

				}
				else{
					$(".late-image").html("<img src='"+path+"'>");
					// $(".late-image").html("<img src='../img/Full-Lash-Volume-Mascara.png'>");
					$('#lateModal').modal('show');
					// $('#lateModal').css('padding-right', '0');
				}				
			});
	    }
	})
}
function connectFacebook(){
	var elmnt = document.getElementById("calendar-list");
    elmnt.scrollIntoView({behavior: "instant", block: "end", inline: "end"});
	if(localStorage.getItem("user") == "" || localStorage.getItem("user") == undefined || localStorage.getItem("user") == null){
		FB.login(function(response){
		  // Handle the response object, like in statusChangeCallback() in our demo
		  // code.
			if (response.status === 'connected'){
				var access_token = response.authResponse.accessToken;
				var fb_id = response.authResponse.userID;
				FB.api('/me', {fields: 'name,email'}, function(response) {
			    	var name= response.name;
			    	var email= response.email;
			    	saveUser(access_token, fb_id,name,email);
			    });
			    $('#fbLoginModal').modal('hide');
			}else{
				
			}
		},{
			scope: 'public_profile,email',
			return_scopes: true
		});
	}else{
		checkResult();
	}	
}

function saveUser(token, id, name, email){
	$.ajax({
		type:'POST',
		url:'/save-user',
		data:{
			token: token,
			id: id,
			name: name,
			email: email,
			day: calendar_click
		},
		success: function(data){
			localStorage.setItem("user",id);
			checkStatus();
			triggerPopup(calendar_click);
	    }
	})
}

function shareFacebook(option, modal){
	var fb_id = localStorage.getItem('user');
	var link = "https://shiseidochristmas.com/";
	var img_path = "";
	var og_image_path = "";
	var og_description = "";
	if (modal == "winModal"){
		img_path = $('.win-image img').attr('src');
	}
	console.log(img_path);
	if (img_path == null || img_path == ""){
		og_image_path = "img/Holiday-46_2000px.png";
		og_description = "Shiseido is giving out exciting prizes every day until Christmas! Try your luck daily and you might win the perfect gift. T&C apply.";
	}else{
		og_image_path = img_path;
			if(og_image_path.startsWith('../')){
				og_image_path = og_image_path.substring(3);
			}
		og_description = "Hey, i have won prizes from Shiseido, have you try your luck?";
	}
	var og_image = link+og_image_path;
	console.log(og_image);
	FB.ui({
	    // method: 'share_open_graph',
	    // action_type: 'og.likes',
	    // action_properties: JSON.stringify({
    	// 	object:{
    	// 		'og:url': link,
    	// 		'og:title': 'Shiseido | #BeautyIsAGift',
    	// 		'og:description': og_description,
    	// 		'og:image': og_image,
    		// }
		// })
		method: 'share',
    	mobile_iframe: true,
    	href: link,
	}, function(response){
		if (response && !response.error_code) {
			$.ajax({
	  			type:'POST',
	  			url:'/save-share-post',
	  			data:{
	  				fb_id: fb_id,
	  				link: link
				}, success: function(data){
  				$('#'+modal).modal('hide');
		  		if (option == 'form'){
		  			// $('#submitModal').modal('show');
		  			$('#submitModal').modal({backdrop: 'static', keyboard: false});
		  			// $('#submitModal').css('top', '30%');
		  		}else{
		  			$('#thankYouModal').modal('show');
		  			// $('#loseHolidayModal').css('top', '30%');
		  		}
		  		}
				})	  
		} else if (response && response.error_code === 4201) {
			console.log("User cancelled!");
		} else {
	        console.log("Not OK!");
	    }		
	});
}

function shareFacebookHeader(){
	var fb_id = localStorage.getItem('user');
	var link = window.location.href;
	FB.ui({
    	method: 'share',
    	mobile_iframe: true,
    	href: 'https://shiseidochristmas-2018.developer.webqlo.com/',
	}, function(response){
		if (response && !response.error_code) {
	        console.log("OK!");
		    $.ajax({
	  			type:'POST',
	  			url:'/save-share-post',
	  			data:{
	  				fb_id: fb_id,
	  				link: link
	  			},
	  			success: function(data){
	  				$('#thankYouModal').modal('show');
	  			}
	  		})	

	    } else if (response && response.error_code === 4201) { //Cancelled
	        console.log("User cancelled!");
	    } else {
	        console.log("Not OK!");
	    }
	});
}

function checkResult(){
	var id = localStorage.getItem("user");
	$.ajax({
		type:'POST',
		url:'/get-product',
		data:{
			fb_id: id,
			day: calendar_click
		},
		success: function(data){
			gtag('event','played',{
					event_category: 'click'
				});
			$.each(data, function(k,v){
				if(k != "error"){
					if( k == "early"){		
						$('#loseHolidayModal').modal('show');
						// $('#loseHolidayModal').css('top', '30%');
					}else if( k == "lose"){
						$('#thankYouModal').modal('show');
					}else if(k=="played"){
						$('#happyHolidayModal').modal('show');
					}
					else{
						if(v == "lose"){
							$('#loseModal').modal('show');
							$('#date-'+k).css("background-image","url(../img/open_calendar/"+k+".png)");
						}else{
							$('.prize-name').html(v['image_name']);
							$('.win-image').html("<img src='"+v['image_title']+"'>");
							$('.submit-gift').html("<img src='"+v['image_title']+"'>");
							$('#date-'+k).css("background-image","url(../img/open_calendar/"+k+".png)");
							$('#winModal').modal('show');
							// $('#winModal').modal({backdrop: 'static', keyboard: false});
						}
						
					}	
				}else{
					$('#loseModal').modal('show');
					// $('#loseModal').css('top', '30%');
				}
			});
		}
	})

}

function AddGift(){
	var name = $('#gift_name').val();
	var path = $('#gift_path').val();
	var description = $('#gift_description').val();
	var slug = $('#slug').val();
	$.ajax({
		type:'POST',
		url:'/insert-image',
		data:{
			name: name,
			path: path,
			description: description,
			slug: slug
		},
		success: function(data){
	        $('#gift_name').val('');
			$('#gift_path').val('');
			$('#gift_description').val('');
			$('#slug').val('');
	    }
	})
}

function AddProduct(){
	var id = $('#product_id').val();
	var quantity = $('#product_quantity').val();
	$.ajax({
		type:'POST',
		url:'/insert-product',
		data:{
			id: id,
			quantity: quantity
		},
		success: function(data){
			console.log(data);

		}
	})
}
var cache = 0;
function openTnc(option){
	if (option == 'form'){
		$('#submitModal').modal('hide');
		cache = 1;
	}
	$('#tncModal').modal('show');	
}

function openPnp(){
	$('#submitModal').modal('hide');
	$('#pnpModal').modal('show');
	cache = 1;
}

function backAction(form, option){
	if (cache == 1){
		$('#'+form).modal('hide');
		$('#submitModal').modal({backdrop: 'static', keyboard: false});
		cache = 0;
	}	
	// window.history.back();
}

function submitForm(e){
	e.preventDefault();
	// return false;
	var fb_id = localStorage.getItem("user");
	var user_name = $('#user_name').val();
	var phone_number = $('#phone_number').val();
	var user_email = $('#user_email').val();
	var user_address = $('#user_address').val();
	var collect_location = $('#collect_location').val();
	var agreement = $('#agreement').prop('checked');
	$("[id$='_error_message']").css('display','none');
	$.ajax({
		type:'POST',
		url:'/submit-form',
		data:{
			user_name: user_name,
			phone_number: phone_number,
			user_email: user_email,
			user_address: user_address,
			collect_location: collect_location,
			agreement: agreement,
			fb_id: fb_id
		},
		success: function(data){
			gtag('event','claimable',{
					event_category: 'win'
				});
			$.each(data, function(k,v){
				if (k=='success'){
					$('#submitModal').modal('hide');
					$('#happyHolidayModal').modal('show');
					// $('#happyHolidayModal').css('top','30%');
				}else{
					$('#submit-form').removeAttr('disabled');
					$('#'+k+"_error_message").html(v);
					$('#'+k+"_error_message").css('display','block');
				}
			});
		}
	})
}
function submitNewForm(e,fb_name,day,gift_id){
	e.preventDefault();
	// return false;
	var user_name = $('#user_name').val();
	var phone_number = $('#phone_number').val();
	var user_email = $('#user_email').val();
	var user_address = $('#user_address').val();
	var collect_location = $('#collect_location').val();
	var agreement = $('#agreement').prop('checked');
	$("[id$='_error_message']").css('display','none');
	$.ajax({
		type:'POST',
		url:'/submit-new-form',
		data:{
			user_name: user_name,
			phone_number: phone_number,
			user_email: user_email,
			user_address: user_address,
			collect_location: collect_location,
			agreement: agreement,
			gift_id: gift_id,
			fb_name: fb_name,
			day: day
		},
		success: function(data){
			$.each(data, function(k,v){
				if (k=='success'){
					$('#happyHolidayModal').modal({backdrop: 'static', keyboard: false});
					// $('#happyHolidayModal').css('top','30%');
				}else{
					$('#submit-form').removeAttr('disabled');
					$('#'+k+"_error_message").html(v);
					$('#'+k+"_error_message").css('display','block');
				}
			});
		}
	})
}

