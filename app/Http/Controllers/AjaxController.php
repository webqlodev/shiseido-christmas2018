<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\GiftList;
use App\FacebookProfile;
use App\UserProfile;
use App\DailyDraw;
use App\DailyClaim;
use App\ShareRecord;
use App\LoginRecord;
use \Datetime;
use DB;
use Validator;
use Mail;


class AjaxController extends Controller
{
    public function save_user(Request $request){
    	$check_fb = DB::table('facebook_profile')->where('fb_id', $request->id)->first();
    	if($check_fb == null){
    		$facebook = new FacebookProfile();
	    	$facebook->name = $request->name;
	    	$facebook->email = $request->email;
	    	$facebook->fb_id = $request->id;
	    	$facebook->access_token = $request->token;
	    	$facebook->save();
    	}
    	return response()->json(['success'=>'success']);
    }

    public function insert_image(Request $request){
    	$gift = new GiftList();
    	$gift->image_name = $request->name;
    	$gift->image_title = $request->path;
    	$gift->image_description = $request->description;
    	$gift->slug = $request->slug;
    	$gift->save();

    	return response()->json(['success'=>'success']);
    }

    public function insert_product(Request $request){
    	$product = new DailyDraw();
    	$product->gift_id = $request->id;
    	$product->quantity = $request->quantity;
    	$product->start_date = date("Y-m-d")." 00:00:00";
    	$product->end_date = date("Y-m-d")." 23:59:59";
    	$product->save();

		return response()->json(['success'=>'success']);    	
    }

    public function messages(){
	    return [
	        'user_name.required' => 'testing',
	    ];
	}

    public function submit_form(Request $request){

    	$validator = Validator::make($request->all(),[
    		'user_name'=>'required',
            'phone_number' => 'required|Min:10|Max:11',
            'user_email' => 'required|email',
            'user_address' => 'required',
            'collect_location' => 'required|not_in:0',
            'agreement' => 'accepted'
    	]);

    	if ($validator->fails()){
    		// dd($validator->errors());
    		return response()->json($validator->errors());
    	}else{
    		$check_prize_won = DB::table('daily_claim')->where([
	    		['fb_id','=',$request->fb_id],
	    		['claim_time','like', '%'.Carbon::now()->toDateString().'%'],
	    	])->first();
	    	// dd($check_prize_won);
    		$get_gift_detail = DB::table('gift_list')->where('id',$check_prize_won->gift_id)->first();
	    	// $get_gift_detail = DB::table('gift_list')->where('id','4')->first();
	    	// dd($get_gift_detail);
    		$get_fb_name = DB::table('facebook_profile')->where('fb_id',$request->fb_id)->first();

    		$info = new UserProfile();
	    	$info->name = $request->user_name;
	    	$info->phone_number =  $request->phone_number;
	    	$info->email = $request->user_email;
	    	$info->address = $request->user_address;
	    	$info->location = $request->collect_location;
	    	$info->gift_name = $get_gift_detail->image_name;
	    	$info->fb_name = $get_fb_name->name;
	    	$info->save();

	    	$string_split = explode("/",$get_gift_detail->image_title); 

	    	$gift_image = $string_split[1].'/'.$string_split[2].'/'.$string_split[3];
	    	$winner_email = $request->user_email;
	    	$data = array(
	    		'email'=>$request->user_email,
	    		'user_name' => $request->user_name,
	    		'phone_number' => $request->phone_number,
	    		'address' => $request->user_address,
	    		'pick_up_counter' => $request->collect_location,
	    		'gift_name' => $get_gift_detail->image_name,
	    		'gift_image' => $gift_image
	    	);
	    	Mail::send(['html'=>'emails.notices'], $data, function($message) use ($data){
	         $message->to($data['email'])->subject
	            ('Congratulations!');
	         $message->from('no-reply@shiseidochristmas.com','Shiseido Malaysia');
	      });


	    	// $data = array('name'=>"Virat Gandhi");
		    //   Mail::send('emails.notice', $data, function($m) {
		    //      $m->to('nguchengen@gmail.com', 'Tutorials Point')->subject
		    //         ('Laravel Testing Mail with Attachment');
		    //      $m->from('michaelngu@webqlo.com','Virat Gandhi');
		    //   });

	    	return response()->json(['success'=>'success']);
    	}
    }

    public function get_product(Request $request){

    	$check_login_record = DB::table('login_record')->where([
    		['fb_id','=',$request->fb_id],
    		['day','=',$request->day],
    		['created_at','like', '%'.Carbon::now()->toDateString().'%'],
    	])->first();
    	$check_lose = DB::table('daily_claim')->where([
    		['fb_id','=',$request->fb_id],
    		['day','=',$request->day],
    		['gift_id','=',0],
    	])->first(); 
    	$check_date = DB::table('daily_draw')->where('day',$request->day)->first();

    	$time_now = date("Y-m-d H:i:s");
    	$time_now = new DateTime($time_now);
		$start_time = new DateTime($check_date->start_date);

    	if ($time_now > $start_time){
    	// if ($check_date == null){
    		if($check_login_record == null){
	    		$result = $this->lottery($request->day,$request->fb_id);
	    		$login_record = new LoginRecord();
	    		$login_record->fb_id = $request->fb_id;
	    		$login_record->day = $request->day;
	    		$login_record->save();


	    		$prize_won = new DailyClaim();
	    		$prize_won->fb_id = $request->fb_id;
		    	$prize_won->day = $request->day;
		    	$prize_won->claim_time = Carbon::now()->toDateTimeString();
	    		if ($result != null){
			    	
			    	$prize_won->gift_id = $result->id;
			    	
					$prize_won->save();    
			    	return response()->json([$request->day=>$result]);
	    		}else{
	    			// $prize_won = new DailyClaim();
	    			$prize_won->gift_id = 0;
	    			// $prize_won->fb_id = $request->fb_id;
	    			// $prize_won->day = $request->day;
			    	// $prize_won->claim_time = Carbon::now()->toDateTimeString();
					$prize_won->save();
	    			return response()->json([$request->day=>'lose']);
	    		}
	    	}else{
	    		if($check_lose == null){
	    			return response()->json(['played'=>'Already play today']);
	    		}else{
	    			return response()->json(['lose'=>'Already lose today']);
	    		}
	    	}
    	}else{
    		// $test = Carbon::now()->toDateTimeString();
    		// dd($test);
    		$campaign_day_today = DB::table('daily_draw')->where([
	    		['start_date','like', '%'.Carbon::now()->toDateString().'%'],
	    	])->first();

	    	$check_login_record = DB::table('login_record')->where('day',$campaign_day_today->day)->first();
	    	if($check_login_record == null){
	    		$result = $this->lottery($request->day,$request->fb_id);

	    		if ($result != null){
	    			$login_record = new LoginRecord();
		    		$login_record->fb_id = $request->fb_id;
		    		$login_record->day = $campaign_day_today->day;
		    		$login_record->save();

			    	$prize_won = new DailyClaim();
			    	$prize_won->gift_id = $result->id;
			    	$prize_won->fb_id = $request->fb_id;
			    	$prize_won->day = $request->day;
			    	$prize_won->claim_time = Carbon::now()->toDateTimeString();
					$prize_won->save();    
			    	return response()->json([$campaign_day_today->day=>$result]);
	    		}else{
	    			$prize_won = new DailyClaim();
	    			$prize_won->gift_id = 0;
	    			$prize_won->fb_id = $request->fb_id;
	    			$prize_won->day = $request->day;
			    	$prize_won->claim_time = Carbon::now()->toDateTimeString();
					$prize_won->save();
	    			return response()->json([$prize_won->day=>'lose']);
	    		}
	    	}else{
	    		return response()->json(['early'=>'early']);
	    	}
    	}
  
    }

    public function get_gift_detail(Request $request){
    	
    }

    public function lottery($day,$fb_id){
		$result = false;
		$unavailable_id = array();

		while($result == false){
			$prize = DB::table('daily_draw')->whereNotIn('gift_id',$unavailable_id)->where('day', $day)->inRandomOrder()->first();
			if ($prize != null){
				$availability_checking = DB::table('daily_claim')->where(
					[
			    		['gift_id',$prize->gift_id],
			    		['day', $day],
			    	]
				)->count();
				// dd($availability_checking, $prize, );
				if($availability_checking >= $prize->quantity){
					$result = false;
					array_push($unavailable_id,$prize->gift_id);
				}else{	
					$check_won_record = DB::table('daily_claim')->where([
						['fb_id','=',$fb_id],
						['gift_id','!=',0],
					])->count();
					//lower down percentage if won many time
					// var_dump($check_won_record);
					if($check_won_record == 0){
						$jackpot = rand(1,2);
					}else if($check_won_record >= 1 && $check_won_record < 5){
						$jackpot = rand(1,4);
					}else if($check_won_record >= 5 && $check_won_record <= 10){
						$jackpot = rand(1,10);
					}else{
						$jackpot = rand(1,20);
					}

					if ($jackpot == 1){
						$result = true;
						$prize_details = DB::table('gift_list')->where('id', $prize->gift_id)->first();
						return $prize_details;
					}else{
						return null;
					}
					// $result = true;
					// $prize_details = DB::table('gift_list')->where('id', $prize->gift_id)->first();
					// return $prize_details;
				}
			}else{
				return null;
			}	
		}
    }

    public function auto_open($day, $id){
    	$check_status = DB::table('login_record')->where([
    		['fb_id','=',$id],
    		['created_at','like', '%'.Carbon::now()->toDateString().'%'],
    	])->first();
    	// dd($check_status);
    	if ($check_status == null){
    		$result = $this->lottery($day, $id);

    		if ($result != null){
    			$get_day = DB::table('daily_draw')->where([
		    		['start_date','like', '%'.Carbon::now()->toDateString().'%'],
		    	])->first();

    			$login_record = new LoginRecord();
	    		$login_record->fb_id = $id;
	    		$login_record->day = $get_day->day;
	    		$login_record->save();

		    	$prize_won = new DailyClaim();
		    	$prize_won->gift_id = $result->id;
		    	$prize_won->fb_id = $id;
		    	$prize_won->day = $day;
		    	$prize_won->claim_time = Carbon::now()->toDateTimeString();
				$prize_won->save();    
		    	return response()->json(['won'=>$result]);
    		}else{
    			$prize_won = new DailyClaim();
    			$prize_won->gift_id = 0;
    			$prize_won->fb_id = $id;
    			$prize_won->day = $day;
		    	$prize_won->claim_time = Carbon::now()->toDateTimeString();
				$prize_won->save();
    			return response()->json(['lose'=>'Didnt win anything today']);
    		}
    	}else{
    		return response()->json(['early'=>'early']);
    	}
    }

    public function check_status(Request $request){
    	$check_status = DB::table('login_record')->where('fb_id',$request->fb_id)->orderBy('id','desc')->first();
    	if( date('m') != 12){
    		return response()->json(["early" => 25]);
    	}
    	else{
	    	if ($check_status != null){
	    		return response()->json(["day"=>$check_status->day]);
	    	} 
    	}
    }

    public function get_campaign_status(){
    	$time_now = Carbon::now()->toDateTimeString();
    	$check_campaign = DB::table('daily_draw')->where('end_date',"<",$time_now)->select('day')->distinct()->get();
    	// dd ($check_campaign);
    	return response()->json(["data"=>$check_campaign]);
    }

    public function get_expired_gift(Request $request){
    	$end = DB::table('campaign')->where('key', 'end_time')->value('value');
    	$date = Carbon::parse($end)->format('j F Y');
    	$check_claimed = DB::table('daily_claim')->where([
    		['fb_id','=',$request->fb_id],
    		['day','=',$request->day],
    	])->first();
    	$check_lose = DB::table('daily_claim')->where([
    		['fb_id','=',$request->fb_id],
    		['day','=',$request->day],
    		['gift_id','=',0],
    	])->first(); 

    	$get_gift = DB::table('daily_draw')->where('day',$request->day)->inRandomOrder()->first();
    	$gift_details = DB::table('gift_list')->where('id',$get_gift->gift_id)->first();
    	$played = "Played";
    	if ( Carbon::parse($end)->isFuture() ) {
	    	if(is_null($check_claimed)){
	    		return response()->json(["img"=>$gift_details->image_title]);
	    	}
	    	else{
	    		if(is_null($check_lose)){
	    			return response()->json(["claimed"=>$played]);
	    		}else{
	    			return response()->json(["lose"=>"lose day"]);
	    		}
	    	}
	    }
	    else{
	    	return response()->json(["end"=>$date]);
	    }
    }

    public function find_gift_path(Request $request){
    	$check_gift_id = DB::table('daily_claim')->where([
    		['fb_id', '=', $request->fb_id],
    		['day','=',$request->day],
    	])->first();
    	$get_gift_path = DB::table('gift_list')->where('id',$check_gift_id->gift_id)->first();

    	if(is_null($get_gift_path)){
    		return 0;
    	}else{
    		return $get_gift_path->image_title;
    	}
    }

    public function save_share_post(Request $request){
    	$save_record = new ShareRecord();
    	$save_record->fb_id = $request->fb_id;
    	$save_record->link = $request->link;
    	$save_record->created_time = Carbon::now()->toDateTimeString();
    	$save_record->save();
    }

}
