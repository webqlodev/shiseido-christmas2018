<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use Illuminate\Http\Request;
use App\Mail\RegistrationVerified;
use Carbon\Carbon;
use App\NewRegistration;
use App\NotFillList;
use App\FacebookProfile;

class HomeController extends Controller
{
    #region Active Period
    public function index()
    {
        // $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        // $gift_detail = DB::table('daily_draw')->orderBy('id','desc')->select('gift_id')->first();
        // return response()->json(["success"=>$gift_detail]);
        $counters = array(
            'SARAWAK' => ['PARKSON THE SPRING SHOPP. MALL', 'PARKSON SIBU', 'PARKSON MIRI'],
            'SABAH' => ['METROJAYA KOTA KINABALU', 'PARKSON IMAGO'],
            'CENTRAL & NORTHERN' => ['AEON KINTA CITY', 'AEON QUEENSBAY MALL', 'PARKSON GURNEY PLAZA','PARKSON SUNWAY CARNIVAL', 'PACIFIC TAIPING MALL'],
            'EAST COAST' => ['PARKSON EAST COAST MALL', 'PACIFIC KB MALL', 'PACIFIC MENTAKAB'],
            'KLANG VALLEY' => ['AEON MID VALLEY MEGAMALL', 'AEON BANDAR UTAMA', 'AEON METRO PRIMA KEPONG', 'AEON TAMAN MALURI', 'AEON BUKIT TINGGI, KLANG', 'PARKSON PAVILION', 'PARKSON K.L.C.C', 'PARKSON SUBANG', 'PARKSON SEREMBAN PARADE', 'SOGO KL', 'SUNWAY PYRAMID BOUTIQUE STORE'],
            'SOUTHERN' => ['AEON BANDARAYA MELAKA', 'AEON DESA TEBRAU', 'AEON BUKIT INDAH JB', 'METROJAYA KOMTAR JBCC', 'PARKSON SQUARE ONE', 'PARKSON HOLIDAY PLAZA']
        );
        // if ( Carbon::parse($end)->isFuture() ) {
        return view('main', ['counters' => $counters]);
            // return view('main');
        // } else {
        //     return view('end', ['end'=>$end] );
        // }
    }
    #endregion
    // public function upload_excel(Request $request){
    //     $file = fopen("file/Not_send_list.csv", "r");
    //     // dd($file);
    //     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
    //     {
    //         $sql = DB::table('not_fill_list')->insert([
    //             'day' => $emapData[0],
    //             'email' => $emapData[1],
    //             'fb_name' => $emapData[2],
    //             'gift_name' => $emapData[3],
    //         ]);
    //     }
    //     fclose($file);
    //     echo 'CSV File has been successfully Imported';

    // }
    public function resend_form_email (){
        $all_users = DB::table('not_fill_list')->get();
        $max = sizeof($all_users);
        for($i=1;$i<=$max;$i++){
            $not_fill_users = DB::table('not_fill_list')->where('id',$i)->first();
            $get_gift_id = DB::table('gift_list')->where('image_name',$not_fill_users->gift_name)->first();
            $email_url = "https://shiseidochristmas.com/form".$not_fill_users->email_code;
            $string_split = explode("/",$get_gift_id->image_title); 
            $image_path = $string_split[1].'/'.$string_split[2].'/'.$string_split[3];
            // dd($email_url);
            $data = array(
                'email' => $not_fill_users->email,
                'url' => $email_url,
                'image_path' => $image_path,
                'gift_name' => $not_fill_users->gift_name 
            );
            Mail::send(['html'=>'emails.new-form-email'], $data, function($message) use ($data){
             $message->to($data['email'])->subject
                ('CLAIM YOUR GIFT HERE!');
             $message->from('no-reply@shiseidochristmas.com','Shiseido Malaysia');
            });
            $find_users = NotFillList::findOrFail($i);
            $find_users->check_sent = 1;
            $find_users->save();
        }
    }
    public function generate_email_code (){
        $all_users = DB::table('not_fill_list')->get();
        $max = sizeof($all_users);
        // dd($max);
        for($i=1;$i<=$max;$i++){
            $not_fill_users = DB::table('not_fill_list')->where('id',$i)->first();
            // dd($not_fill_users);
            $get_fb_id = DB::table('facebook_profile')->where('email',$not_fill_users->email)->first();
            $get_gift_id = DB::table('gift_list')->where('image_name',$not_fill_users->gift_name)->first();
            // dd($not_fill_users->day,$get_fb_id->fb_id,$get_gift_id->id);
            $email_code = "?a=".$get_gift_id->id."&b=".$get_fb_id->fb_id."&c=".$not_fill_users->day;
            // dd($email_code);
            $find_users = NotFillList::findOrFail($i);
            $find_users->email_code = $email_code;
            $find_users->save();
        }
    }

    public function new_form (Request $request){
        if($request->has('a')){
            $valid_prize = DB::table('daily_claim')->where([
                ['gift_id','=',$request->a],
                ['fb_id','=',$request->b],   
                ['day','=',$request->c],
            ])->first();
           
            if(empty($valid_prize) || $valid_prize === null){
               return redirect()->to('/'); 
            }
            
            $fb_name = DB::table('facebook_profile')->where('fb_id',$request->b)->first();
            $gift = DB::table('gift_list')->where('id',$request->a)->first();
            $validation = DB::table('new_registration')->where([
                ['day','=',$request->c],
                ['fb_name','=',$fb_name->name],
                ['gift_name','=',$gift->image_name],
            ])->first();
            // dd($valid_prize,$validation);
            if($validation === null || empty($validation)){
                $counters = array(
                'SARAWAK' => ['PARKSON THE SPRING SHOPP. MALL', 'PARKSON SIBU', 'PARKSON MIRI'],
                'SABAH' => ['METROJAYA KOTA KINABALU', 'PARKSON IMAGO'],
                'CENTRAL & NORTHERN' => ['AEON KINTA CITY', 'AEON QUEENSBAY MALL', 'PARKSON GURNEY PLAZA','PARKSON SUNWAY CARNIVAL', 'PACIFIC TAIPING MALL'],
                'EAST COAST' => ['PARKSON EAST COAST MALL', 'PACIFIC KB MALL', 'PACIFIC MENTAKAB'],
                'KLANG VALLEY' => ['AEON MID VALLEY MEGAMALL', 'AEON BANDAR UTAMA', 'AEON METRO PRIMA KEPONG', 'AEON TAMAN MALURI', 'AEON BUKIT TINGGI, KLANG', 'PARKSON PAVILION', 'PARKSON K.L.C.C', 'PARKSON SUBANG', 'PARKSON SEREMBAN PARADE', 'SOGO KL', 'SUNWAY PYRAMID BOUTIQUE STORE'],
                'SOUTHERN' => ['AEON BANDARAYA MELAKA', 'AEON DESA TEBRAU', 'AEON BUKIT INDAH JB', 'METROJAYA KOMTAR JBCC', 'PARKSON SQUARE ONE', 'PARKSON HOLIDAY PLAZA']
                );
                return view('new-form',[
                    'counters' => $counters, 
                    'gift_id' => $request->a,
                    'day' => $request->c,
                    'img_path' => $gift->image_title,
                    'fb_name' => $fb_name->name,
                ]);
            }else{
                return redirect()->to('/');
            }
        }
        else{
            return redirect()->to('/'); 
        }  
    }

    public function submit_new_form (Request $request){
        $validator = Validator::make($request->all(),[
            'user_name'=>'required',
            'phone_number' => 'required|Min:10|Max:11',
            'user_email' => 'required|email',
            'user_address' => 'required',
            'collect_location' => 'required|not_in:0',
            'agreement' => 'accepted'
        ]);

        if ($validator->fails()){
            // dd($validator->errors());
            return response()->json($validator->errors());
        }else{
            $get_gift_detail = DB::table('gift_list')->where('id',$request->gift_id)->first();
            $info = new NewRegistration();
            $info->name = $request->user_name;
            $info->phone_number =  $request->phone_number;
            $info->email = $request->user_email;
            $info->address = $request->user_address;
            $info->location = $request->collect_location;
            $info->gift_name = $get_gift_detail->image_name;
            $info->fb_name = $request->fb_name;
            $info->day = $request->day;
            $info->save();

            $string_split = explode("/",$get_gift_detail->image_title); 

            $gift_image = $string_split[1].'/'.$string_split[2].'/'.$string_split[3];
            $winner_email = $request->user_email;
            $data = array(
                'email'=>$request->user_email,
                'user_name' => $request->user_name,
                'phone_number' => $request->phone_number,
                'address' => $request->user_address,
                'pick_up_counter' => $request->collect_location,
                'gift_name' => $get_gift_detail->image_name,
                'gift_image' => $gift_image
            );
            Mail::send(['html'=>'emails.notices'], $data, function($message) use ($data){
             $message->to($data['email'])->subject
                ('Congratulations!');
             $message->from('no-reply@shiseidochristmas.com','Shiseido Malaysia');
            });
            return response()->json(['success'=>'success']);
        }
    }

    #region Submit Form
    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:registrations,email',
            'location' => 'required',
        ]);

        if ( $validator->fails() ) {
            return back()->withErrors($validator)->withInput();
        }

         // Get code from code table

         $codeObj = DB::table('codes')->where('used', false)->first();
         if ($codeObj){
             $code = $codeObj->code;
             DB::table('codes')->where('code', $code)->update(['used' => true]);
         }
         else{ // handle when all code used.
             $code = $this->generateUniqueCode();
             DB::table('codes')->insert([
                 'code' => $code,
                 'used' => true,
             ]);
         }

        // Save new registration to database

        $registrationId = DB::table('registrations')->insertGetId([
            'fullname' => $request->fullname,
            'email' => $request->email,
            'redeem_location' => $request->location,
            'unique_code' => $code,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        // Send email

        Mail::to([$request->fullname => $request->email])
             ->send(new RegistrationVerified($request, $code));

        return redirect()->route('thank-you', [
            'unique_code' => $code,
        ]);
    }
    #endregion

    #region ThankYou
    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        
        if ($registration) {
            return view('thank-you', ['unique_code' => $code,'registration'=>$registration]);
        } else {
            abort(404);
        }
    }
    #endregion

    public function generateUniqueCode()
    {
        $codeCharacters = 'BCDFGHJKLMNPQRSTVWXYZ0123456789';
        while (true) {
            $code = '';
            for ($i = 0; $i < 5; $i++) {
                $code .= $codeCharacters[mt_rand(0, strlen($codeCharacters) - 1)];
            }

            // Check if the generated code exists
            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }
        return $code;
    }

    public function test_email(){
        return view('emails.notice'); 
    }

    #region Redirect
    public function redirect($location)
    {
        DB::table('clicks')->insert([
            'location' => $location,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        switch ($location) {
            case 'facebook':
                $url = 'https://www.facebook.com/lauramerciercosmeticsmalaysia/';
                break;
        }
        return redirect($url);
    }
    #endregion
}
