<?php

namespace App\Http\Controllers;

use DB;
use Datatables;
use Excel;
use Mail;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        $campaign_time['start'] = $start;
        $campaign_time['end'] = $end;
        $social = [
            'facebook' => DB::table('clicks')->where('location', 'facebook')->count(),
        ];

        return view('admin',['campaign_time' => $campaign_time, 'social' => $social]) ; 

    }

    public function saveDate(Request $request)
    {
        DB::table('campaign')
            ->where('key', 'start_time')
            ->update(['value' => $request->start]);
        DB::table('campaign')
            ->where('key', 'end_time')
            ->update(['value' => $request->end]);
        $response = array(
            'status' => 'success',
            'msg' => 'Date saved.',
            'start' => $request->start,
            'end' => $request->end,
        );
        return response()->json($response);
    }

    public function DatatablesTotalRegistration()
    {
        $total = new Collection;
        // $startDate = new Carbon( env('START_TIME') );
        // $endDate = new Carbon( env('END_TIME') );
        $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        $startDate = new Carbon( $start );
        $endDate = new Carbon( $end );

        // Loop through registrations table by date

        $date = $startDate;

        while ( $date->lte($endDate) && !$date->isTomorrow() ) {
            $total->push([
                'date' => $date->format('j F Y (l)'),
                'total' => DB::table('daily_claim')->whereBetween( 'created_at', [
                    $date->toDateString(),
                    $date->addDay()->toDateString(),
                ])->count(),
            ]);
        }

        $total->reverse();

        // Reorder to descending order
        $total = $total->reverse();

        return Datatables::of($total)->make(true);
    }

    public function datatablesTotalSharingRecord(){
    
        $fb_account = DB::table('facebook_profile')
        ->select(['facebook_profile.email','facebook_profile.name','facebook_profile.fb_id'])
        ->join('share_record','facebook_profile.fb_id','=','share_record.fb_id') 
        ->get();
        
        $count = [];

        foreach ($fb_account as $value) {
            if(!isset($count[$value->fb_id])) {
                $count[$value->fb_id]['email']=$value->email;
                $count[$value->fb_id]['name']=$value->name;
                $count[$value->fb_id]['count']=1;
                $count[$value->fb_id]['fb_id']=$value->fb_id;
            } else {
                $count[$value->fb_id]['count']++;
            }
        }

        return Datatables::of($count)->make(true);
    }

    public function datatablesRegistrationList()
    {
        $registrations = DB::table('user_profile')->select([
            'id',
            'email',
            'fb_name',
            'gift_name',
            'check_up',
            'created_at as time',
        ])->orderBy('created_at', 'desc')->get();

        $registrations->transform(function ($item, $key) {
            $time = new Carbon($item->time);
            $item->time = $time->format('j F Y (l) h:i a');
            return $item;
        });

        return Datatables::of($registrations)
        // ->addColumn('checkbox',function($registrations){
        //     return '<input type="checkbox" id="'.$registrations->check_up.'" name="claimcheck" />';
        // })
        // ->rawColumns(['action','checkbox'])
        ->make(true);
    }
    public function datatablesNotfilledList(){
        $registrations = DB::table('new_registration')->select([
            'id',
            'email',
            'fb_name',
            'gift_name',
            'check_up',
            'day',
        ])->orderBy('day', 'desc')->get();

        return Datatables::of($registrations)->make(true);
    }

    public function datatablesGiftList()
    {
        $gift_list = DB::table('daily_draw')
        ->select([
            'gift_list.image_name',
            'daily_draw.start_date',
            'daily_draw.quantity'])
        ->join('gift_list','gift_list.id','=','daily_draw.gift_id')
        ->orderBy('daily_draw.start_date') 
        ->get();
        // echo '<pre>';
        // print_r($gift_list);
        // die();
        return Datatables::of($gift_list)->make(true);
    }

    public function resendEmail($code)
    {
        
        HomeController::makeVoucher($code);
        Mail::to( DB::table('user_profile')->where('id', $code)->value('email') )->send( new NoticeEmail($code) );

        return 'Email sent. <button onclick="window.close()">Close</button>';
    }

    public function checkClaim(Request $request)
    {
        $user_profile = DB::table('user_profile')
        ->where('id',$request->id)
        ->update(['check_up' => 1]);
    }
    public function exportfbshareList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('user_profile', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Register Email',
                    'Facebook Name',
                    'Facebook Name',
                    'Total Shared',
                ]);

                $fb_account = DB::table('facebook_profile')
                ->select(['facebook_profile.email','facebook_profile.name','facebook_profile.fb_id'])
                ->join('share_record','facebook_profile.fb_id','=','share_record.fb_id') 
                ->get();
                $count = [];
                foreach ($fb_account as $value) {
                    if(!isset($count[$value->fb_id])) {
                        $count[$value->fb_id]['email']=$value->email;
                        $count[$value->fb_id]['name']=$value->name;
                        $count[$value->fb_id]['count']=1;
                        $count[$value->fb_id]['fb_id']=$value->fb_id;
                    } else {
                        $count[$value->fb_id]['count']++;
                    }
                }
                foreach ($count as $value) {
                    $str_fb_id = strval($value['fb_id']);
                    $rowIndex++;
                    $sheet->row($rowIndex, [
                        $value['email'],
                        $value['name'],
                        $str_fb_id,
                        $value['count'],
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');   
    }

    public function exportRegistrationList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('user_profile', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Email Address',
                    'Location',
                    'FB Name',
                    'Gift',
                    'Register Time',
                    'Claimed',
                ]);

                $registrations = DB::table('user_profile')->orderBy('id', 'asc')->get();
                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);

                    $sheet->row($rowIndex, [
                        $value->email,
                        $value->location,
                        $value->fb_name,
                        $value->gift_name,
                        $time->format('j F Y (l) h:i a'),
                        $value->check_up = ($value->check_up == 1) ? 'Yes' : 'No',
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
    public function exportNotfilledList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('new_registration', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Email Address',
                    'Location',
                    'FB Name',
                    'Gift',
                    'Register Date',
                    'Claimed',
                ]);

                $registrations = DB::table('new_registration')->orderBy('id', 'asc')->get();
                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);

                    $sheet->row($rowIndex, [
                        $value->email,
                        $value->location,
                        $value->fb_name,
                        $value->gift_name,
                        $value->day." December 2018",
                        $value->check_up = ($value->check_up == 1) ? 'Yes' : 'No',
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
}
