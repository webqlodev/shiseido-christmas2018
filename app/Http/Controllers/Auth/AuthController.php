<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;
use App\User;

class AuthController extends Controller
{
    public function redirectToFacebook()
   {
       return Socialite::driver('facebook')->redirect();
   }

    public function handleFacebookProvider()
    {
        try {
           $user = Socialite::driver('facebook')->user();

           // $existing_user = User::where('fb_id',$user->getId())->first();
           // if($existing_user!=null) {
           //     Auth::loginUsingId($existing_user->id);



           //     return redirect('/member-privilege');
           // } else {
           //     $create['name'] = $user->getName();
           //     $create['email'] = $user->getEmail();
           //     $create['fb_id'] = $user->getId();
           //     $create['fb_token'] = $user->token;
           //     $create['password'] = "";
           //     $create['avatar'] = $user->getAvatar();
           //     $create['role_id'] = "3";
           //     $create['status'] = "1";

           //     $userModel = new User;
           //     $createdUser = $userModel->addNew($create);
           //     Auth::loginUsingId($createdUser->id);



           //     return redirect('/member-privilege');
           // }
            $testing = $user->getName();
            return redirect('/', compact('testing'));
       } catch (Exception $e) {
           if (isset($_GET['error_reason']) && $_GET['error_reason'] == 'user_denied') {
               return redirect('/');
           }

           var_dump($e);
           // return redirect('auth/facebook');
       }
    }
}
