<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyDraw extends Model
{
    protected $table="daily_draw";

    protected $fillable=[
       'start_date', 'end_date', 'gift_id', 'quantity', 'day'
   	];
}
