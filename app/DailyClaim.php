<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyClaim extends Model
{
    protected $table="daily_claim";
}
