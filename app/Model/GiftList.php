<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftList extends Model
{
    protected $table="gift_list";

    protected $fillable=[
       'image_name', 'image_title', 'slug'
   	];
}
