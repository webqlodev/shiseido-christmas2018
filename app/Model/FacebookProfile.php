<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookProfile extends Model
{
    protected $table="facebook_profile";
}
