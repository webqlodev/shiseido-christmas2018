<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationVerified extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The variable instances.
     *
     * @var Request
     * @var Verification Code
     */
    public $request;
    public $verificationCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $verificationCode)
    {
        $this->request = $request;
        $this->verificationCode = $verificationCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->from('no-reply@shiseidosuncare.com.my', 'LAURA MERCIER | #CityOfLights')            
            ->subject('Laura Mercier #CityOfLights')
            ->view('emails.to-user');            
    }
}
