<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareRecord extends Model
{
    protected $table="share_record";
}
