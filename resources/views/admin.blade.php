@extends('layouts.admin')

@section('content')
<div class="row">
    @if ( Auth::user()->name == 'webqlo' || Auth::user()->name == 'foo' )

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-10">
                <blockquote>
                    <strong>Start Time: </strong><span id="start-time">{{ Carbon\Carbon::parse( $campaign_time['start'] )->format('j F Y, H:i') }}</span>
                    <br>
                    <strong>End Time: </strong><span id="end-time">{{ Carbon\Carbon::parse( $campaign_time['end'] )->format('j F Y, H:i') }}</span>
                </blockquote>
            </div>
            <button type="button" class="btn btn-primary col-xs-2" data-toggle="modal" data-target="#campaign_date_modal">
                Edit
            </button>
        </div>
    </div>
    @if ( Auth::user()->name == 'foo' )
    <div class="col-xs-12">
        <h3>Insert Gift Image Details Here:</h3>
        <div id="shiseido-gift" class="row">
            <div class="form-group col-xs-6">
                <input type="text" name="gift_name" id="gift_name" placeholder="Gift Name">
            </div>
            <div class="form-group col-xs-6">
                <input type="text" name="gift_path" id="gift_path" placeholder="Gift Image Path">
            </div>
            <div class="form-group col-xs-6">
                <input type="text" name="gift_description" id="gift_description" placeholder="Gift Description">
            </div>
            <div class="form-group col-xs-6">
                <input type="text" name="slug" id="slug" placeholder="Slug">
            </div>
            <div class="col-xs-12 text-right"><button type="submit" class="btn btn-primary" onclick="AddGift();">Submit</button></div>
        </div>
    </div>
    <div class="col-xs-12">
        <h3>Insert Daily Draw Products Here:</h3>
        <div id="daily-draw-product" class="row">
            <div class="form-group col-xs-6">
                <input type="text" name="product_id" id="product_id" placeholder="Product id">
            </div>
            <div class="form-group col-xs-6">
                <input type="number" name="product_quantity" id="product_quantity" placeholder="Quantity">
            </div>
            <div class="col-xs-12 text-right"><button type="submit" class="btn btn-primary" onclick="AddProduct();">Submit</button></div>
        </div>
    </div>
    <div class="col-xs-12"><br/></div>
    @endif
    @endif
    <div class="col-md-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Participant by Date</strong></div>

            <div class="panel-body">
                <table id="total_registration_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Total Registration by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Facebook Share Record</strong></div>
            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-fbshare-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="facebook_share_record" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Register Email</th>
                            <th>Facebook Name</th>
                            <th>Facebook ID</th>
                            <th>Total Shared</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@if ( Auth::user()->name == 'foo' )
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Gift List</strong></div>
            <div class="panel-body">
                <table id="gift_list" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Gift Name</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Winner Detail List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-registration-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="registration_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Email Address</th>
                            <th>Facebook Name</th>
                            <th>Gift Name</th>
                            <th>Register Time</th>
                            <th>Tick for Claimed</th>
                            @if ( Auth::user()->name == 'foo' )                           
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Not Fill Winner Detail List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-notfilled-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="notfilled_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Email Address</th>
                            <th>Facebook Name</th>
                            <th>Gift Name</th>
                            <th>Register Time</th>
                            <th>Tick for Claimed</th>
                            @if ( Auth::user()->name == 'foo' )                           
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('modal')
<div class="modal fade" id="campaign_date_modal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Edit Campaign Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Please select the start and end date of the campaign.</p>
                <p>
                    Example:
                    <br>
                    Start date is 1 April 2018 00:00:00am, End date is 30 April 2018 at 11:59:59pm.
                    <br>
                    Just choose <b>1 April 2018</b> and <b>30 April 2018</b> will do.
                </p>
                <div class="input-daterange input-group" id="datepicker">
                    <input id="start_date" type="text" class="input-sm form-control" name="start" />
                    <span class="input-group-addon">to</span>
                    <input id="end_date" type="text" class="input-sm form-control" name="end" />
                </div>
                <button id="save_btn" type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush

@push('js')
<script>
$(function () {
    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#save_btn').on('click', function (e) {
        e.preventDefault();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var start_date_time = start_date + 'T00:00:00+08:00';
        var end_date_time = end_date + 'T23:59:59+08:00';
        var start_html = new Date(start_date_time);
        var end_html = new Date(end_date_time);
        $.ajax({
            type:'POST',
            data: {_token: CSRF_TOKEN, start: start_date_time, end: end_date_time},
            url: '{{ route('save-date') }}',
            dataType: 'JSON',
            success: function(data) {
                alert(data.msg);
                $('#start-time').html(moment(start_date_time).format('DD MMM YYYY, HH:mm'));
                $('#end-time').html(moment(end_date_time).format('DD MMM YYYY, HH:mm'));
            }
        });
    });

    $('#total_registration_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('total-registration') }}',
        columns: [
            {data: 'date', name: 'date', orderable: false},
            {data: 'total', name: 'total', orderable: false}
        ]
    });

    $('#facebook_share_record').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('total-sharing-record') }}',
        columns: [
            {data: 'email'},
            {data: 'name'},
            {data: 'fb_id'},
            {data: 'count'},
        ]
    });

    $('#gift_list').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('gift-list') }}',
        columns: [
            {
                data: 'start_date'
            },
            {
                data: 'image_name'
            },
            {
                data: 'quantity'
            }
        ]
    });

    $('#notfilled_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('notfilled-list') }}',
        order: [],
        columns: [
            {
                data: 'email'
            },
            {
                data: 'fb_name'
            },
            {
                data: 'gift_name'
            },
            {
                data: 'day'
            },
            {
                render: function(data, type, row, meta) {
                    if (row.check_up == 1){
                        return '<input type="checkbox" value="' + row.id + '" checked disabled/>';
                    }
                    else {
                        return '<input class="claimcheck" type="checkbox" value="' + row.id + '"/>';
                    }
                }, orderable: false, searchable: false

            }
            @if ( Auth::user()->name == 'foo' )
            ,
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/resend-email/' + row.id + '" target="_blank">Resend Email</a>';
                }, orderable: false, searchable: false
            }
            @endif
        ]
    });

    var table = $('#notfilled_list_table').DataTable();

        table.on('draw', function(){
        $(".claimcheck").bind("change",function() {
        var element = $(this);
        var dateRange = element.val();
        if(element.is(':checked')) {
            $.ajax({
                url: '/check-claim',
                type: 'GET',
                data: { id: dateRange },
                success: function(data){
                    element.attr('disabled',true);
                }
            });
        }
        });
    });

    $('#registration_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('registration-list') }}',
        order: [],
        columns: [
            {
                data: 'email'
            },
            {
                data: 'fb_name'
            },
            {
                data: 'gift_name'
            },
            {
                data: 'time'
            },
            {
                render: function(data, type, row, meta) {
                    if (row.check_up == 1){
                        return '<input type="checkbox" value="' + row.id + '" checked disabled/>';
                    }
                    else {
                        return '<input class="claimcheck" type="checkbox" value="' + row.id + '"/>';
                    }
                }, orderable: false, searchable: false

            }
            @if ( Auth::user()->name == 'foo' )
            ,
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/resend-email/' + row.id + '" target="_blank">Resend Email</a>';
                }, orderable: false, searchable: false
            }
            @endif
        ]
    });
    
    var table = $('#registration_list_table').DataTable();

        table.on('draw', function(){
        $(".claimcheck").bind("change",function() {
        var element = $(this);
        var dateRange = element.val();
        if(element.is(':checked')) {
            $.ajax({
                url: '/check-claim',
                type: 'GET',
                data: { id: dateRange },
                success: function(data){
                    element.attr('disabled',true);
                }
            });
        }
        });
    });
});

</script>
@endpush