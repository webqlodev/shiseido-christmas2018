<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89145400-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-89145400-11');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    
    <title>{{ config('app.name', 'Shiseido Christmas 2018') }}</title>
    <meta name="description" content="Laura Mercier Holiday 2018 Collection themed City of Lights captures the vivid colours and glamour of the holidays in the city imbued in an alluring festive glow."/>
    <meta name="keywords" content="lauramercier, malaysia, cityoflights, face, palette, skincare, brush, eye, lip"/>
    <link rel="icon" type="image/png" href="{{asset('img/favicon.ico')}}">

    <!-- SEO Keywords -->
    <meta property="fb:app_id" content="328636391049493" />
    <meta property="og:url"           content="http://shiseidochristmas-2018.developer.webqlo.com/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Shiseido | #BeautyIsAGift" />
    <meta property="og:description"   content="Shiseido is giving out exciting prizes every day until Christmas! Try your luck daily and you might win the perfect gift. T&C apply." />
    <meta property="og:image"         content="{{asset('img/Holiday-46_2000px.png')}}" />

    <!-- Styles -->
    <!-- <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" /> -->
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.4.1/css/all.css">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/bootstrap-responsive-tabs.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/ovewrite.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body class="snow-wrap">
    @yield('content')
    <!-- Scripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.bootstrap-responsive-tabs.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/jquery.snowfall.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/phaser.min.js') }}"></script>
    @yield('script')
    @include('tnc')
    @include('privacy-laura')
    @include('consent')
    <script>  
      $.ajaxSetup({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
      });

      var url_string = window.location.href.split('/');
      var redirect_string = url_string[3].split('#');

      if (redirect_string[1] == 'terms'){
          $('#tncModal').modal('show');
      }

      checkStatus();
      // checkCampaignStatus();
      console.log(localStorage.getItem("user"));

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '722173871485289',  //live
          // appId      : '328636391049493', //local
          cookie     : true,
          xfbml      : true,
          version    : 'v3.2'
        });
          
        FB.AppEvents.logPageView();   
          
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

</body>
</html>
