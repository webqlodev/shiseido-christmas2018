@php
$whatsapp_url = 'https://wa.me/?text=https://shiseidochristmas.com/';
@endphp
<div class="page-header">
    <div class="container">
        <div class="nav-menu">
            <div class="row m-0">
                <div class="col-sm-3 col-md-3 col-lg-4">
                    <div class="header-menu-outer-container">
                        <div class="header-menu-inner-container text-left">
                            <a href="."><img src="{{ asset('img/Shiseido_logo.png') }}"></a>
                        </div>
                    </div>  
                </div>
                <div class="col-sm-9 col-md-9 col-lg-8 p-0">
                    <div class="header-menu-outer-container custom-margin-top">
                        <div class="header-menu-inner-container">
                            <ul class="m-0 custom-text-color">
                                <li><a class="menu-item" href="#about">ABOUT</a></li>
                                <li><a class="menu-item" href="#calendar">CALENDAR</a></li>
                                <li><a class="menu-item" href="#product">PRODUCTS</a></li>
                                <li class="sharing-option">
                                    <span class="custom-display menu-item"><span><i class="fas fa-share-alt" style="    font-size: 0.85em;"></i> SHARE</span></span>
                                    <span class="img-container">
                                        <img class="sharing-list" src="{{ asset('img/box.png') }}">
                                        <a href="#" onclick="shareFacebookHeader('share','header');"><img class="fb-img" src="{{ asset('img/FB.png') }}"></a>
                                        <a href="{{$whatsapp_url}}" target="_blank"><img class="whatsapp-img" src="{{ asset('img/whatsappimg.png') }}"></a></span>
                                </li>
                            </ul>
                        </div>   <!-- .header-menu-inner-container --> 
                    </div><!-- .header-menu-outer-container -->
                </div>
            </div>  <!-- row -->
        </div> <!-- nav-menu -->
    </div> <!-- container -->
</div> <!-- page-header -->

<div class="mobile-page-header text-center">
    <div class="mobile-header-container">
        <img style="width: 150px;" src=" {{asset('img/Shiseido_logo.png')}} ">
        <img class="mobile-menu-icon open-menu" onclick="toggleImage();">
    </div>
    <div class="mobile-menu container">
        <div class="row m-0">
            <div class="col-12 text-left mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#about">ABOUT</a></div>
            <div class="col-12 text-left mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#calendar">CALENDAR</a></div>
            <div class="col-12 text-left mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#product">PRODUCTS</a></div>
            <div class="col-12 text-left custom-mobile-padding"><div class="mobile-menu-item"><i class="fas fa-share-alt" style="font-size: 0.85em;"></i><span class="mobile-share-text">SHARE</span>
                <a href="#" onclick="shareFacebookHeader('share','header');"><img class="custom-margin-left mobile-menu-img" src="{{asset('img/FB.png')}}">
                <a href="{{$whatsapp_url}}"><img class="custom-margin-left mobile-menu-img" src="{{asset('img/whatsappimg.png')}}"></a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- mobile-page-header -->