<div class="footer">
    <div class="container">
        <div class="row m-0">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg">
                <div class="footer-menu-outer-container">
                    <div class="footer-menu-inner-container-image">
                        <a href="."><img src="{{ asset('img/Shiseido_logo_footer.png') }}"></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg footer-menu-list p-0">
                <!-- <div class="row m-0">
                    <div class="col-3 text-center">
                        <span class="footer-menu text-white">Terms and Conditions</span>
                    </div>
                    <div class="col-3 text-center">
                        <span class="footer-menu text-white">Follow Us:</span>
                    </div>
                    <div class="col-3 text-center">
                        <span class="footer-menu"><img src="{{ asset('img/facebook.png') }}"></span>
                    </div>
                    <div class="col-3 text-center">
                        <span class="footer-menu"><img src="{{ asset('img/youtube.png') }}"></span>
                    </div>
                </div> -->
                <div class="footer-menu-outer-container custom-margin-top">
                    <div class="footer-menu-inner-container">
                        <ul class="m-0 text-white">
                            <li><a class="footer-item" href="#" onclick="openTnc('direct');">Terms and Conditions</a></li>
                            <li>Follow Us:</li>
                            <li><a class="footer-item" href="https://www.facebook.com/shiseido.malaysia/" target="_blank"><i class="fab fa-facebook" style="font-size: 2em;vertical-align: -0.2em;"></i></a></li>
                            <li><a class="footer-item" href="https://www.youtube.com/channel/UCYTlZzQINEfwcQFq-yJv7cQ" target="_blank"><i class="fab fa-youtube" style="font-size: 2em;vertical-align: -0.2em;"></i></a></li>
                        </ul>
                    </div>    
                </div>
            </div>        
        </div>
    </div>    
</div>