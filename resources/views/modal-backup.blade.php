<!-- modal -->

                                <div id="fbLoginModal" class="modal fade" role="dialog" style="position: absolute;">
                                    <div class="modal-dialog fb-login-modal-dialog" >
                                        <!-- Modal content-->
                                        <div class="modal-content fb-login-modal-content">
                                            <button type="button" class="close fb-login-close" data-dismiss="modal">&times;</button>
                                            <div class="modal-body fb-login-modal-body">
                                                <div style="display: table;width: 100%;height: 100%;">
                                                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                                                        <div class="fb-login-title">Hold On There</div>
                                                        <p class="fb-login-content">Thank you for your interest in Shiseido's gifting season. Please login with your Facebook to proceed.</p><br>
                                                        <!-- <p><a class="fb-login-button" href="{{ url('/auth/facebook') }}"><i class="fab fa-facebook-f"></i><span style="margin-left: 2rem;">Login with Facebook</span></a></p> -->
                                                        <p><button class="fb-login-button" onclick="connectFacebook();"><i class="fab fa-facebook-f"></i><span style="margin-left: 2rem;">Login with Facebook</span></button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- fbloginmodal -->

                                <div id="happyHolidayModal" class="modal fade" role="dialog" style="position: absolute;">
                                    <div class="modal-dialog happy-holiday-modal-dialog" >
                                        <!-- Modal content-->
                                        <div class="modal-content happy-holiday-modal-content">
                                            <button type="button" class="close happy-holiday-close" data-dismiss="modal">&times;</button>
                                            <div class="modal-body happy-holiday-modal-body">
                                                <div style="display: table;width: 100%;height: 100%;">
                                                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                                                        <div class="happy-holiday-title">Happy Holidays!</div>
                                                        <p class="happy-holiday-content">Thank you for taking the time to check out the Shiseido advent calendar. Please check your e-mail inbox for a copy of your submission details. Be sure to come back tomorrow for more exciting surprises.</p><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- happyholidaymodal -->
                                <div id="sorryModal" class="modal fade" role="dialog" style="position: absolute;">
                                    <div class="modal-dialog sorry-modal-dialog" >
                                        <!-- Modal content-->
                                        <div class="modal-content sorry-modal-content">
                                            <button type="button" class="close sorry-close" data-dismiss="modal">&times;</button>
                                            <div class="modal-body sorry-modal-body">
                                                <div style="display: table;width: 100%;height: 100%;">
                                                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                                                        <div class="sorry-title">We are sorry</div>
                                                        <p class="sorry-content">Unfortunately you didn't pick up anything today, however, it's not over yet. Share the joy via the Facebook button below and you can stand a chance to win other exciting prizes.</p><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- sorrymodal -->
                                <div id="winModal" class="modal fade" role="dialog" style="position: absolute;">
                                    <div class="modal-dialog win-modal-dialog" >
                                        <!-- Modal content-->
                                        <div class="modal-content win-modal-content">
                                            <button type="button" class="close win-close" data-dismiss="modal">&times;</button>
                                            <div class="modal-body win-modal-body">
                                                <div style="display: table;width: 100%;height: 100%;">
                                                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                                                        <div class="win-image">[gift won]</div>
                                                        <div class="win-title">Congratulations!</div>
                                                        <p class="win-content">You've won yourself a [name of gift set]! Be sure to spread the joy and share this on Facebook to be eligible for the prize. Head back here afterwards to provide us with your personal information.</p><br>
                                                        <p><div class="fb-login-button" onclick="shareFacebook();"  data-href="https://shiseidochristmas2018.com/" data-layout="button_count"><i class="fab fa-facebook-f"></i><span style="margin-left: 2rem;">Share</span></div></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- winmodal -->
                                <div id="submitModal" class="modal fade" role="dialog" style="position: absolute;">
                                    <div class="modal-dialog submit-modal-dialog" >
                                        <!-- Modal content-->
                                        <div class="modal-content submit-modal-content">
                                            <button type="button" class="close submit-close" data-dismiss="modal">&times;</button>
                                            <div class="modal-body submit-modal-body">
                                                <div style="display: table;width: 100%;height: 100%;">
                                                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                                                        <div class="submit-gift">[gift image]</div>
                                                        <div class="submit-title">You're Almost There!</div>
                                                        <p class="submit-content">Please provide your details below</p>

                                                        <form method="post" action="">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="user_name" placeholder="Name">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="number" class="form-control" id="phone_number" placeholder="Phone number">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" id="user_email" placeholder="Email Address">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="user_address" placeholder="address">
                                                            </div>
                                                            <p>I  would like to collect my gift at</p>
                                                            <div class="form-group">
                                                                <select class="form-control" id="collect_location">
                                                                    <option>Please choose one</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary" onclick="submitForm">Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- submitmodal -->