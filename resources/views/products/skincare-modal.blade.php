<div id="skincare-desk" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button class="close" type="button" data-dismiss="modal">×</button>
        <div id="skincaremodalcarousel" class="carousel">
        <div class="carousel-inner">

          <div class="item active">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/skincare_desk/ambrevanille.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                    Luxe Indulgences Ambre Vanillé Luxe Body Collection
                <br>rm359
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                       Set Includes :<br>
                  Crème Body Wash 100ml, Soufflè Body Crème 200ml, Eau De Toilette 15ml, Honey Bath 200ml & Wooden Honey Dipper
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      A limited edition collection featuring an assortment of Ambre Vanillé Body & Bath favorites.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/skincare_desk/almondcoconutmilk.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      luxe indulgences almond coconut milk luxe body collection 
                <br>rm359
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                       Set Includes :<br>
                  Crème Body Wash 100ml, Soufflè Body Crème 200ml, Eau De Toilette 15ml, Honey Bath 200ml & Wooden Honey Dipper
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                        <br>
                A limited edition collection featuring an assortment of Almond Coconut Milk Body & Bath favorites.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/skincare_desk/soufflecreme.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                    Luxe Indulgences Soufflé Body Crème Collection
                <br>rm209
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                  4 x Mini Soufflè Body Crème 60ml each
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      A unique, limited-edition collection features a curated array of Laura’s most iconic Soufflé Body Crèmes in chic mini sizes ideal for travel.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/skincare_desk/hand&body.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      luxe indulgences hand & body crèmes collection 
                <br>rm159
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                          Set Includes :<br>
                  4 x Hand & Body Crème 30ml each
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                Limited-edition collection features a curated array of four Mini Hand and Body Crèmes.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/skincare_desk/infusionderose.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      infusion de rose nourishing collection
                <br>rm495
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                       Set Includes :<br>
                  Infusion de Rose Nourishing Oil, Infusion de Rose Nourishing Crème,Face Polish
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                 A limited edition trio of skincare favorites to soften, visibly smooth and balance skin, featuring Infusion de Rose Nourishing Crème. Infusion de Rose Nourishing Oil and Infusion de Rose Nourishing Lip Balm.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div><!-- /.carousel-inner -->
        <a class="left carousel-control" href="#skincaremodalcarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#skincaremodalcarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
      </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->