<div id="eyes-desk" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button class="close" type="button" data-dismiss="modal">×</button>
        <div id="eyesmodalcarousel" class="carousel">
        <div class="carousel-inner">

          <div class="item active">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('../img/palettes/masterclass.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      master class artistry in light holiday illuminations edition 
                <br>rm495
                    </div>
                    <div class="col-md-12 gotham-book-13pt">
                   <br>
                    A limited edition collection with iconic shades, finishes and liners to create effortless looks.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

             <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('../img/palettes/nightsout.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      nights out eye shadow palette 
                      <br>rm260
                    </div>
                    <div class="col-md-12 gotham-book-13pt">
                   <br>
                An eyeshadow palette featuring 12 dazzling, highly pigmented shades that span from light and luminous to deeply dramatic.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/eyesNlips_desk/shadow&lights.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      shadows & lights mini caviar stick collection 
                      <br>rm170
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                  4 x Caviar Stick Eye Colour in Rosegold, Amethyst, Moonlight & Au Natural 1gm each
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                Shadow, highlight, line and define. Matte and metallic shades glide on smoothly and blend effortlessly for the perfect sultry, smoky eye.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/eyesNlips_desk/shadow&brights.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      shadows & brights metallic caviar stick eye colour collection 
                <br>rm170
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                  4 x Caviar Stick Eye Colour in Intense Amethyst, Intense Rose Gold, Intense Moonlight & Intense Copper 1gm each
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      A limited edition collection of long-lasting, metallic creamy eye shadow sticks that can also line, highlight, and create smoky looks.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/eyesNlips_desk/eyelights.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      eye lights deluxe mini eye collection 
                      <br>rm140
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                       Set Includes :<br>
                      2 x Caviar Stick Eye Colour 1g each & Full Blown Volume Supreme Lash Building Mascara 5.7g
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      Light up your eyes with this exclusive, limited-edition trio of magnetic eye favorites.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/eyesNlips_desk/starlightmini.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                    Starlights Mini Lip Glacé Collection
                      <br>rm165
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                        5 x Lip Glace in Cosmic, Bare Pink, Pink Pop, Rose Gold Accent & BonBon 2.8gm each
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                Five festive, perfectly pigmented lip glosses with high-shine and sheer coverage.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/eyesNlips_desk/liplights.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      lip lights deluxe mini lip collection 
                <br>rm140
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                  Velour Lovers 2.75g, Lip Glace 2.8g & Lip Pencil 1.49g
                      </i>
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                A limited edition lip collection featuring our cult favorites Lip Glace, Lip Colour, and Lip Pencil.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 

        </div><!-- /.carousel-inner -->
        <a class="left carousel-control" href="#eyesmodalcarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#eyesmodalcarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
      </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->