<div id="brush" class="container">
	<div class="row">
		<div id="brush_decs" class="carousel slide" data-ride="carousel" data-interval="false">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#brush_decs" data-slide-to="0" class="active"></li>
		    <li data-target="#brush_decs" data-slide-to="1"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		   		<div class="product-bg-img fix-height">
		   			<div class="products_header gotham-bold-32pt text-spacing">
		   				brush collection
		   			</div>
		   			<div class="swipe gotham-book-13pt">
	   					Swipe to find out more
		   			</div>
		   			<div class="arrow-swipe">
		   				<i class="far fa-long-arrow-alt-right"></i>
		   			</div>
		   		</div>
		   		
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/brush/brushstrokes.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				brush strokes luxe brush collection 
		    				<br>rm375
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Fan Brush, Check Colour Brush, Smoky Eye Liner Brush, All Over Eye Colour Brush, Eye Crease Brush
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				The mirrored palette features 6 matte and 6 sateen-shimmer highly pigmented hues from light and luminous to deeply dramatic. This gorgeous versatile palette takes eyes from neutral day to sultry night making the eyes the focus this season.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Create the perfect holiday look with this luxe collection of Laura's most iconic brushes for flawless application.
		    			</div>
		    		</div>
		    	</div>
		    </div>
		  </div>
		  <a class="left carousel-control" href="#brush_decs" role="button" data-slide="prev">
		  	{{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    		{{--<span class="sr-only">Previous</span>--}}
		  </a>
		  <a class="right carousel-control" href="#brush_decs" role="button" data-slide="next">
		  	{{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    		{{--<span class="sr-only">Next</span>--}}
		  </a>
		</div>
	</div>
</div>
