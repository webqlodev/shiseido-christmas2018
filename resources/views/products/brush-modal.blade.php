<div id="brush-desk" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button class="close" type="button" data-dismiss="modal">×</button>
        <div id="brushmodalcarousel" class="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/brush_desk/brushstrokes.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      brush strokes luxe brush collection 
                      <br>rm375
                    </div>
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br>
                      <i>
                        Set Includes :<br>
                        Fan Brush, Check Colour Brush, Smoky Eye Liner Brush, All Over Eye Colour Brush, Eye Crease Brush
                      </i>
                    </div>--}}
                    {{--<div class="col-md-12 gotham-book-13pt">
                      <br><br>
                      The mirrored palette features 6 matte and 6 sateen-shimmer highly pigmented hues from light and luminous to deeply dramatic. This gorgeous versatile palette takes eyes from neutral day to sultry night making the eyes the focus this season.
                    </div>--}}
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                Create the perfect holiday look with this luxe collection of Laura's most iconic brushes for flawless application.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.carousel-inner -->
        {{--<a class="left carousel-control" href="#brushmodalcarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>--}}
        {{--<a class="right carousel-control" href="#brushmodalcarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>--}}
      </div>
      </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->