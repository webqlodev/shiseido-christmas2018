<div id="desktop-product" class="container">
	<div class="row">
		<div id="face_desk" class="col-xs-4 pb-2">
			<a href="#face-desk" data-toggle="modal">
				<div class="overlay">
					<img class="img-desk" src="{{asset('/img/face_desk/flawlessface_A.jpg')}}" alt="flawlessface"/>
					<div class="upperfont gotham-bold-48pt">holiday <br>flawless face</div>
				</div>
			</a>
		</div>
		<div id="palettes_desk" class="col-xs-4 pb-2">
			<a href="#palettes-desk" data-toggle="modal">
			<div class="overlay">
				<img class="img-desk" src="{{asset('/img/palettes_desk/palette_A.jpg')}}" alt="palettes"/>
				<div class="upperfont gotham-bold-48pt">holiday <br>glow</div>
			</div>
			</a>
		</div>
		<div id="eyesNlips_desk" class="col-xs-4 pb-2">
			<a href="#eyes-desk" data-toggle="modal">
			<div class="overlay">
				<img class="img-desk" src="{{asset('/img/eyesNlips_desk/eyes_A.jpg')}}" alt="eyes&lips"/>
				<div class="upperfont gotham-bold-48pt">holiday <br>eyes & lips</div>
			</div>
			</a>
		</div>
		<div id="skincare_desk" class="col-xs-offset-2 col-xs-4">
			<a href="#skincare-desk" data-toggle="modal">
			<div class="overlay">
				<img class="img-desk" src="{{asset('/img/skincare_desk/skincare_A.jpg')}}" alt="skincare"/>
				<div class="upperfont gotham-bold-48pt">holiday <br>body & bath</div>
			</div>
			</a>
		</div>
		<div id="brush_desk" class="col-xs-4">
			<a href="#brush-desk" data-toggle="modal">
				<div class="overlay">
					<img class="img-desk" src="{{asset('/img/brush_desk/brushes_A.jpg')}}" alt="brushes"/>
					<div class="upperfont gotham-bold-48pt">brush <br>collection</div>
				</div>
			</a>
		</div>
	</div>
@include('products.brush-modal')
@include('products.face-modal')
@include('products.palettes-modal')
@include('products.eyelips-modal')
@include('products.skincare-modal')
</div>

