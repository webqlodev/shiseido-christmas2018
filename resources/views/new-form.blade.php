@extends('layouts.app')
@section('content')
<div id="new-form" class="container-fuild">
    <div class="row">
        <div class="offset-md-2 col-md-8 offset-1 col-10 text-center">
            <div class="submit-gift"><img src="{{ $img_path }}"></div>
            <div class="submit-title modal-title-text">You're Almost There!</div>
            <p class="submit-content">Please provide your details below</p>
            <form>
                <div class="form-group">
                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name">
                    <p id="user_name_error_message" class="text-danger text-left"></p>
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="phone_number"  maxlength="11" placeholder="Phone number">
                    <p id="phone_number_error_message" class="text-danger text-left"></p>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="user_email" placeholder="Email Address">
                    <p id="user_email_error_message" class="text-danger text-left"></p>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="user_address" placeholder="Address">
                    <p id="user_address_error_message" class="text-danger text-left"></p>
                </div>
                <p class="submit-content text-left collect-content">I  would like to collect my gift at</p>
                
                <div class="form-group select-group">
                    <select class="form-control input-medium" id="collect_location">
                        <option value="">Please choose one</option>
                        @foreach($counters as $key => $value)
                        <optgroup label="{{ $key }}">
                            @foreach($value as $mall)
                            <option value="{{ $mall }}">
                                {{ $mall }}
                            </option>
                            
                            @endforeach 
                        </optgroup>
                        @endforeach
                    </select>
                    <p id="collect_location_error_message" class="text-danger text-left"></p>
                </div>
                <div class="form-check text-left">
                    <input class="form-check-input submit-content" type="checkbox" name="agreement" id="agreement" value="agree">
                    <label class="form-check-label agree-term" for="agreement">
                        I have read and agree <a href="#" onclick="openPnp('form');"><u>Privacy Policy</u></a> and <a href="#" onclick="openTnc('form');"><u>Terms and Conditions</u></a> 
                    </label>
                      <!-- <p id="agreement_error_message" class="text-danger"></p> -->
                </div>
                <p class="text-left text-danger" id="agreement_error_message" class="text-danger"></p>
                <button id="submit-form" class="btn submit-button" onclick="this.disabled=true;submitNewForm(event,'{{ $fb_name }}','{{ $day }}','{{ $gift_id }}');">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="happyHolidayModal" class="modal fade" role="dialog">
    <div class="modal-dialog happy-holiday-modal-dialog modal-dialog-centered" >
        <!-- Modal content-->
        <div class="modal-content happy-holiday-modal-content">
            <button type="button" class="close happy-holiday-close" data-dismiss="modal" onclick="closeModal();"><img alt="close" ></button>
            <div class="modal-body happy-holiday-modal-body">
                <div style="display: table;width: 100%;height: 100%;">
                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                        <div class="happy-holiday-title modal-title-text">Happy Holidays!</div>
                        <p class="happy-holiday-content modal-content-text">Thank you for taking the time to check out the Shiseido advent calendar.<br/><b>Please check your e-mail inbox for a copy of your submission details.</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- happyholidaymodal -->

<div id="tncModal" class="modal fade" role="dialog" >
    <div class="modal-dialog tnc-modal-dialog modal-dialog-centered" >
        <!-- Modal content-->
        <div class="modal-content tnc-modal-content">
            <button type="button" class="close tnc-close" data-dismiss="modal" onclick="backAction('tncModal');"><img alt="close" ></button>
            <div class="modal-body tnc-modal-body">
                <div style="display: table;width: 100%;height: 100%;">
                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                        <div class="tnc-title modal-title-text">Terms and Conditions</div>
                        <div class="tnc-content modal-content-text text-justify">
                            <p><strong>Shiseido Beauty Is A Gift Daily Giveaway</strong></p>
                            <p>Please read these Terms and Conditions carefully before taking part in this Daily Giveaway. By participating, you are accepting the stipulated Terms and Conditions and agree to abide by them. If you do not agree to these terms and conditions, please refrain from participating in this Daily Giveaway. The Organiser reserves the right to amend the Terms and Conditions at any time without prior notice.</p>
                            <br>
                            <p><strong>Giveaway Information</strong></p>
                            <p>Shiseido Beauty Is A Gift Daily Giveaway (“Giveaway”) is organised by Shiseido Malaysia Sdn. Bhd. (“Organiser”). The Giveaway begins on 1 December 2018, 12:00AM and ends on 25 December 2018, 11:59PM (“Giveaway Period”).</p>
                            <br>
                            <p>The Organiser reserves the right to vary, extend, suspend, or terminate the giveaway at their sole discretion. Such termination or suspension shall not give rise to any claim by the participant.</p>
                            <br>
                            <p><strong>Giveaway Eligibility</strong></p>
                            <p>1. This Giveaway is open to all Malaysian citizens residing in Malaysia aged 18 and above with a valid Malaysian National Registration Identity Card. Employees, shareholders, officers, directors, agents, distributors, and representatives of the Organiser and each of their respective parent companies, affiliates, divisions, subsidiaries, agents, representatives and advertising agencies, together with the immediate family members (regardless of where they live) and those living in the same household of such persons (whether or not related), are not eligible to participate in the Giveaway.</p>
                            <br>
                            <p><strong>Giveaway Mechanics</strong></p>
                            <p>2. In order to participate and be eligible to win during this giveaway, the participant must:</p>
                            <ol type="a">
                                <li>Visit <a href="https://shiseidochristmas.com">https://shiseidochristmas.com</a> and click on the calendar section.</li>
                                <br>
                                <li>The selection and quantity of gifts are sorted in a random arrangement. Lucky winners will win gifts based on a lucky draw method.</li>
                                <br>
                                <li>Winners must share the gift post on FB and fill in required data. Only then will they be eligible to collect their winnings from selected Shiseido counters.</li>
                                <br>
                                <li>Individuals that did not win can share the giveaway post on FB and will be entered to a separate lot who stand a chance to win surprise gifts.</li>
                                <br>
                                <li>All gifts must be collected at the specific preferred Shiseido Counter as registered on the website, where the confirmed counter location may not be changed.</li>
                                <br>
                                <li>Gift collection period: 28 Feb – 31 March 2019. Any uncollected gift will be forfeited after 31 March 2019.</li>
                            </ol>
                            <br>
                            <p><strong>Prizes</strong></p>
                            <ol>
                                <li>Each Winner may win more than 1 gift.</li>
                                <br>
                                <li>All Gifts are awarded “as is” and are strictly non-transferable, non-exchangeable and may not be encashed.</li>
                                <br>
                                <li>In the event that a Winner has not provided their relevant details within 14 days upon being notified by the Organiser, or if contacted, chooses not to accept the Gift, the Organiser reserves the right to disqualify the Participant’s eligibility and forfeit the Gift.</li>
                                <br>
                                <li>The Organiser does not guarantee the availability of the Gift and the Organiser has the sole and absolute discretion to replace and/or substitute such Gift(s) with any other Gift of similar value as determined by the Organiser.</li>
                                <br>
                                <li>All Gifts are awarded by the Organiser on an “as is” basis and are accepted by the Participant without warranty or guarantee of any kind, whether expressed or implied. Where requested by the Organiser, the Participant shall execute a deed of release and indemnity in a form prescribed by the Organiser.</li>
                            </ol>
                            <br>
                            <p><strong>Terms and Conditions</strong></p>
                            <ol>
                                <li>By participating in this Giveaway, the Participant is deemed to have read, understood, agreed, and unconditionally accepted the Terms and Conditions and the Privacy Notice stated below.</li>
                                <br>
                                <li>The Participant warrants, undertakes and agrees that the Organiser shall have the sole right to use or exploit the Participant’s Entry or any part thereof in any and all form of media by any and all manner or means throughout the world for the full period of copyright including all renewals together with the right to authorise others so to do without having to pay any remuneration or royalties to the Participant. For the avoidance of doubt, the Organiser is under no obligation to use, or exploit any part thereof.</li>
                                <br>
                                <li>The Organiser reserves the right to at any time, change, amend, delete, or add to the Terms and Conditions and other rules and regulations including the mechanism of the Giveaway at its absolute discretion. The Organiser also reserves its absolute right and discretion to cancel, postpone, shorten, or extend this Giveaway (including operating hours and days) and also may revoke or withhold any Participant’s eligibility for the Giveaway.</li>
                                <br>
                                <li>The Organiser reserves the right to disqualify and/or exclude Participants and/or revoke the Gifts (at any stage of the Giveaway) if:</li>
                                    <ol type="a">
                                        <li>the Participant is ineligible or does not meet any of the eligibility criteria set out in the Terms and Conditions; or</li>
                                        <br>
                                        <li>the Participant breaches the Terms and Conditions or other rules and regulations of the Giveaway or violated any applicable laws or regulations; or</li>
                                        <br>
                                        <li>in the Organiser’s sole determination, it believes that the Participant has attempted to undermine the operation of the Giveaway by fraud, cheating or deception;</li>
                                    </ol>
                                <br>
                                <li>Whilst the Organiser will endeavour to conduct necessary verifications on the eligibility of participants, failure to disqualify any ineligible participants shall not be deemed a breach by the Organiser.</li>
                                <br>
                                <li>The Participant shall not dispute nor make any oral or written complaints, public announcements, or statements on the same whether during or after the Giveaway Period.</li>
                            </ol>                                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- tncModal -->

<div id="pnpModal" class="modal fade" role="dialog">
    <div class="modal-dialog pnp-modal-dialog modal-dialog-centered" >
        <!-- Modal content-->
        <div class="modal-content pnp-modal-content">
            <button type="button" class="close pnp-close" data-dismiss="modal" onclick="backAction('pnpModal');"><img alt="close" ></button>
            <div class="modal-body pnp-modal-body">
                <div style="display: table;width: 100%;height: 100%;">
                    <div style="display: table-cell; vertical-align:middle; text-align: center;">
                        <div class="pnp-title modal-title-text">Privacy Policy</div>
                        <div class="pnp-content modal-content-text text-justify">
                            <p>Shiseido Malaysia Sdn. Bhd. (<strong>“Shiseido”</strong> or <strong>“We”</strong>) is committed to protecting your privacy and ensuring that your Personal Data is protected. For the purposes of this Privacy Policy, "Personal Data" means any personally identifiable data, whether true or not, about an individual who can be identified from that data.</p>
                            <br>
                            <h3>1. APPLICATION</h3>
                            <br>
                            <p>This Privacy Policy explains the types of Personal Data we collect and how we use, disclose, transfer, process and protect that information.</p>
                            <br>
                            <p>We collect Personal Data through, but not limited to, the following means:</p>
                            <br>
                            <ol type="a">
                                <li>When you shop on or browse [https://www.shiseido.com.my] (the <strong>“Website”</strong>);
                                </li>
                                <li>When you shop in-store at our physical stores;</li>
                                <li>When you connect with us through social media or attend our marketing events; and</li>
                                <li>When you agree and consent to be a member of the Shiseido Membership, whether through
                                    physical or electronic means.
                                </li>
                            </ol>
                            <br>
                            <p>We may update this Privacy Policy from time to time by posting updated versions on the Website, and/or by sending an e-mail to you. Your continued membership, access to and/or use of the Website will be taken to be your agreement to, and acceptance of, all changes made in each updated version.<br>Please check back regularly for updated information on how we handle your Personal Data.</p>
                            <br>
                            <h3>2. CONSENT</h3><br>
                            <p>We do not collect, use or disclose your Personal Data without your consent (except where permitted and authorised by law). By providing your Personal Data to us, you hereby consent to us collecting, using, disclosing, transferring, and processing your Personal Data for the purposes set out in Section 3 of this Privacy Policy.</p>
                            <br>
                            <p>The types of Personal Data we collect include, but are not limited to, your: (a) first name and family name; (b) home address; (c) date of birth; (d) email address; and, only if appropriate, your (e) user name and password; (f) billing and delivery address; (g) personal identification number; and (h) other information as may be reasonably required for us to provide you with the Services as defined in Section 3 below.</p>
                            <br>
                            <h3>3. PURPOSE</h3>
                            <p>We collect, use, disclose, transfer and process your Personal Data for the purpose of providing services. These services include, but are not limited to:</p>
                            <br>
                            <ol>
                                <li>providing you with information on products and campaigns from us, Shiseido Group and our third party business partners via email, SMS, and post (where we have your express consent);</li>
                                <li>allowing you to purchase products and services offered for sale via the Website;</li>
                                <li>facilitating your transactions with us;</li>
                                <li>sending you product samples and/or products;</li>
                                <li>keeping you informed of updates, changes, and developments relating to us and our Services;</li>
                                <li>notifying you about important changes to this Privacy Policy, and to our other policies or services;</li>
                                <li>providing you with personalized consultations;</li>
                                <li>responding to queries or feedback from you;</li>
                                <li>maintaining and operating the Website;</li>
                                <li>managing our administrative and business operations;</li>
                                <li>engaging third party business partners and data processors to perform certain aspects of the Services;</li>
                                <li>performing customer profiling, market analysis, and research to improve our product and service offerings to you;</li>
                                <li>preventing, detecting and investigating crime and analysing and managing commercial risks; and </li>
                                <li>other purposes which are reasonably related to the above.</li>
                            </ol>
                            <br>
                            <p>(collectively, the <strong>"Services"</strong>)</p>
                            <br>
                            <h3>4. WITHDRAWAL OF CONSENT, ACCESS & CORRECTION</h3>
                            <br>
                            <p>If you wish to withdraw your consent to receive information on new products and campaigns, or any other Services, you may do so by:</p>
                            <br>
                            <ol>
                                <li>unsubscribing from our Website;</li>
                                <li>clicking the “Unsubscribe" link in the email(s) we send to you;</li>
                                <li>contacting our Data Protection Officer at the email address below; or</li>
                                <li>writing to us at the address below.</li>
                            </ol>
                            <br>
                            <p>Please note that if you choose not to provide us with certain Personal Data, or to withdraw your consent to our use, disclosure, transfer and/or processing of your Personal Data, we may not be able to provide you with some or all of the Services.</p><br>
                            <p>We will ensure that the Personal Data in our possession is accurate and complete to the best of our knowledge.</p><br>
                            <p>You have a right to request for access and correction of your Personal Data. If you would like assistance in accessing and/or correcting your Personal Data, please contact our Data Protection Officer at the email address below. We will get back to you within 21 days.</p><br>
                            <h3>5. CHILDREN</h3><br>
                            <p>This Website is directed toward and designed for use by persons aged 16 or older. We do not intend to collect Personal Data from children under 16 years of age, except on some sites specifically directed to children.</p><br>
                            <p>We protect the Personal Data of children along with the necessary parental consent in the same manner as it protects the Personal Data of adults.</p><br>
                            <h3>6. THIRD PARTY DISCLOSURE & TRANSFER</h3><br>
                            <p>We do not disclose or transfer your Personal Data to third parties unless we have clearly asked for and obtained your consent to do so (except where permitted and authorised by law).</p><br>
                            <p>The Personal Data which you provide to us may be stored, processed, transferred between, and accessed from servers located in the United States and other countries which have laws and regulations that may not guarantee the same level of protection of Personal Data as Malaysia. However, we will take reasonable steps to ensure that your Personal Data is handled in accordance with this Privacy Policy, regardless of where your Personal Data is stored or accessed from.</p><br>
                            <dl>
                                <dt>6.1 Disclosure to affiliated companies in the Shiseido Group</dt>
                                <dd>The Shiseido Group comprises a number of affiliated companies and legal entities located both within and outside Malaysia. We may disclose, where appropriate and to the extent necessary, your Personal Data to such affiliated companies and legal entities for the purposes of corporate reporting, market research and analysis, customer relationship management and other related legal and business purposes. Please note that we provide our affiliated companies and legal entities with only the Personal Data they need for such business and legal purposes, and we require that they protect such Personal Data in accordance with the applicable laws and regulations and this Privacy Policy, and not use it for any other purpose.
                                </dd><br>
                                <dt>6.2 Disclosure to third party business partners</dt>
                                <dd>We rely on third party business partners to perform a variety of services on our behalf. In so doing, Shiseido may let service providers, located both within and outside Malaysia, use your Personal Data for the marketing and promotion of products, services or events that may be of interest to you, for market research and analysis, for customer relationship management, and for the fulfilment of your orders for products and services purchased via the Website. Please note that we provide our third party business partners with only the Personal Data they need to perform their services and we require that they protect such Personal Data in accordance with the applicable laws and regulations and this Privacy Policy, and not use it for any other purpose.
                                </dd><br>
                                <dt>6.3 Disclosure to third party data processors</dt>
                                <dd>We may use third party service providers, located both within and outside Malaysia, to help us maintain and operate the Website, or for other reasons related to the operation of the Website and Shiseido’s business, and they may receive your Personal Data for these purposes. We only provide them the Personal Data they need to provide these services on our behalf. We require these companies to protect the Personal Data in accordance with the applicable laws and egulations and this Privacy Policy, and to not use the information for any other purpose.
                                </dd><br>
                                <dt>6.4 Other disclosure</dt>
                                <dd>We may use and disclose your Personal Data to perform your instructions and, as relevant, (a) comply with legislative and regulatory requirements; (b) protect or defend rights or properties of customers and employees of Shiseido; and/or (c) take emergency measures for the purpose of securing the safety of customers, Shiseido, or the general public. We may also disclose and transfer our personal data to other service providers and/or third parties (which may be both within and outside Malaysia) in the context of a merger, acquisition or any other corporate exercise involving Shiseido.
                                </dd><br>
                            </dl>
                            <h3>7. SECURITY & PROTECTION</h3><br>
                            <p>We maintain strict procedures, standards, and security arrangements to protect Personal Data in our possession or under our control. Upon receipt of your Personal Data, whether through physical or electronic means of collection, we will make the necessary security arrangements to protect such Personal Data as are reasonable and appropriate in the circumstances. Such arrangements may comprise administrative measures, physical measures, technical measures, or a combination of such measures.</p><br>
                            <p>When disclosing or transferring your Personal Data over the internet, we take all reasonable care to prevent unauthorised access to your Personal Data. However, no data transmission over the internet can be guaranteed as fully secure and you acknowledge that you submit information over the internet at your own risk.</p><br>
                            <h3>8. RETENTION OF PERSONAL DATA</h3><br>
                            <p>We will not retain your Personal Data for any period of time longer than is necessary to serve the purposes set out in this Privacy Policy and any valid business or legal purposes. After this period of time, we will destroy or anonymise any documents containing your Personal Data in a safe and secure manner.</p><br>
                            <h3>9. GOVERNING LAW</h3><br>
                            <p>This Privacy Policy is governed by Malaysia law.</p><br>
                            <h3>10. CONTACT US</h3><br>
                            <p>If you would like to access or correct any Personal Data which you have provided to us, submit a complaint, or have any queries about your Personal Data, please contact our Data Protection Officer by contacting us at <a href="mailto:shiseidocamellia@shiseido.com.my">shiseidocamellia@shiseido.com.my</a>. Alternatively, you may write to us at:<br> Shiseido Malaysia Sdn.Bhd.<br> Unit 7-03, Level 7, Menara UAC, No.12, Jalan PJU 7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor Darul Ehsan,Malaysia<br> Tel:60-3-7719-1888
                            </p><br><br>
                            <p>Date: 20 July 2018 </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- pnpModal -->
@endsection
@section('script')
<script type="text/javascript">
    function closeModal(){
        window.location.href = "{{ route('main') }}"; 
    }
</script>
@endsection