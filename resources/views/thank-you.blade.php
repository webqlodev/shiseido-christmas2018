@extends('layouts.app')
@section('content')
    <div id="thank_section">        
        <div class="container">
            <div id="thank_you" class="row mtop-30">
               <div class="col-md-8 col-md-offset-2">
                    <div class="thank-logo text-center">
                        <a href="{{url('/')}}">
                            <img src="{{url('img/LauraMercier_logo.png')}}" class="img-fluid">
                        </a>
                    </div>
                    <div class="text-left clr pt-0">Dear {{ $registration->fullname }},</div>
                    
                    <div class="text-left text-justify ftx mtop-30">
                        You may redeem this exclusive <span class="highlight">Deluxe Sample</span> by presenting this verification code <span class="highlight">{{ $registration->unique_code }}</span> at your preferred Laura Mercier store location at 
                        <br><span class="highlight">{{ $registration->redeem_location }}</span>
                    </div>
                    <div class="text-left ftx mtop-30">
                        <h4>Following is the info you provided to us:</h4>
                        <br>
                        <ul>
                            <li><span class="highlight">Your Name:</span> {{ $registration->fullname }}</li>
                            <li><span class="highlight">Email address:</span> {{ $registration->email }}</li>
                        </ul>                     
                    </div>
                    <p class="text-left ftx mtop-30">
                       Thank you for your participation and see you soon!
                    </p>

                    <div class="text-center share-buttons">
                        <a class="btn share-button" id="thankshare" href="{{ route('redirect', ['target' => 'facebook']) }}"><i class="fa fa-facebook-official"></i> <span>Share</span></a>
                    </div>

                    <p class="small text-left text-justify mtop-40">
                       Terms & Conditions: 
                        <br />
                        *Limited samples available. One redemption per customer.  Duplicate redemptions will not be entertained. Laura Mercier Malaysia reserves the right to modify or amend the validity of this offer without prior notice. Laura Mercier is available at Pavilion KL, 1Utama, Sunway Pyramid, Gurney Plaza, Aeon Tebrau City JB.
                    </p>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
@endsection
