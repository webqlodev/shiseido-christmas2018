
<div class="modal fade" id="privacy_policy_new" role="dialog">
    <div class="modal-dialog modal-lg">               
      	<div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title" style="text-align: center; font-weight:bold;">Privacy Policy</h4>
            </div>
       		<div class="modal-body">
				<h3>NOTICE AND CONSENT UNDER THE PERSONAL DATA PROTECTION ACT 2010</h3>
				<p class="text-justify">The Personal Data Protection Act 2010 (hereinafter referred to as the “PDPA”), which regulates the processing of personal data in commercial transactions, applies to Shiseido Malaysia Sdn Bhd (hereinafter referred to as “SML”, “our”, “us” or “we”). For the purpose of this written notice, the terms “personal data” and “processing” shall have the meaning prescribed in the PDPA.
				</p>
				<ol class="text-justify">
					<li>
						This written notice serves to inform you that your personal data is being processed by or on behalf of SML.
					</li>
					<li>
						The personal data processed by us may include your name, national registration identity card (NRIC) number or passport number, date of birth, gender, marital status, race, nationality, contact number, postal address, email address, preferred counter name or location and sensitive personal data such as product purchase and return history, sample history, gift redemption history, facial/body history, skin/hair concern, lifestyle, occupation, gross income range and education profile provided by you in the documents as prescribed in paragraph 4 of this notice and all other details relating thereto.
					</li>
					<li>
						<p>We are processing your personal data, including any additional information you may subsequently provide, for the following purposes (“Purposes”):</p>
						<ol type="a">
							<li>
								processing your application in relation to our customer loyalty membership program and purchase of product;
							</li>
							<li>
								managing and administering our records in relation to our customer loyalty membership program;
							</li>
							<li>
								communicating with you about our products and services, in particular about promotional offers, marketing information and event programs
							</li>
							<li>
								facilitating or enabling any checks that we conduct or by any third party on you from time to time;
							</li>
							<li>
								complying with the applicable laws;
							</li>
							<li>
								assisting any government agencies or bureaus or bodies including for the purposes of police or regulatory investigations;
							</li>
							<li>
								facilitating your compliance with any laws or regulations applicable to you;
							</li>
							<li>
								communicating with you and responding to your enquiries;
							</li>
							<li>
								providing services to you;
							</li>
							<li>
								conducting our internal activities, internal market surveys and trend analysis; or such other purposes as may be related to the foregoing.
							</li>
						</ol>
					</li>
					<li>
						Your personal data is and will be collected from you and/or from the information you have provided in your emails, all application/registration forms, information that you may provide us from time to time, and from third parties such as retailers, service providers, banks and insurance companies.
					</li>
					<li>
						Your personal data may be disclosed to parties such as but not limited to auditors, governmental departments and/or agencies, regulatory and/or statutory bodies, our related corporations and/or members of Shiseido Malaysia Sdn Bhd, Warisan TC Berhad, Shiseido Company Limited (Japan) and their respective subsidiaries, business partners, service providers and any such third party requested or authorized by you for any of the above Purposes or any other purpose for which your personal data was to be disclosed at the time of its collection or any other purpose directly related to any of the above Purposes.
					</li>
					<li>
						If you fail to supply to us the above personal data, we may not be able to process your personal data for any of the above Purposes.
					</li>
					<li>
						Your personal data may be transferred to a place outside Malaysia.
					</li>
					<li>
						You are responsible for ensuring that the personal data you provide us is accurate, complete and not misleading and that such personal data is kept up to date.
					</li>
					<li>
						We may request your assistance to procure the consent of third parties whose personal data is provided by you to us and you agree to use your best endeavors to do so.
					</li>
					<li>
						In the event of any inconsistency between the English version and the Bahasa Malaysia version of this notice, the English version shall prevail over the Bahasa Malaysia version.
					</li>
					<li>
						<p>Privacy Preference and How to Contact Us<br>
						You may access and request for correction of your personal data and to contact us with any enquiries or complaints in respect of your personal data as follows:</p>

						<div class="container-fluid">
							<div class="row ppbord text-left"> 
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">Brand</div>
										<div class="col-xs-6 ppbord-inline-1">Laura Mercier</div>
									</div>
								</div>
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">Designation of the contact person</div>
										<div class="col-xs-6 ppbord-inline-1">Laura Mercier Customer Service</div>
									</div>
								</div>
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">Phone number</div>
										<div class="col-xs-6 ppbord-inline-1">+60 3 7719 1888</div>
									</div>
								</div>
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">Fax number</div>
										<div class="col-xs-6 ppbord-inline-1">+60 3 7727 8263</div>
									</div>
								</div>
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">E-mail address</div>
										<div class="col-xs-6 ppbord-inline-1"><a href="mailto:marketing@lauramercier.com.my">marketing@lauramercier.com.my</a></div>
									</div>
								</div>
								<div class="col-xs-12 ppbord-inline">
									<div class="row">
										<div class="col-xs-6">Address</div>
										<div class="col-xs-6 ppbord-inline-1">Unit 7-03 Level 7 Menara UAC, No 12 Jalan PJU7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor Darul Ehsan, Malaysia</div>
									</div>
								</div>
							</div>
						</div>
						<br><br>
						<p>In accordance with the PDPA:</p>
						<ol type="a">
							<li>We may charge a prescribed fee for processing your request for access or correction; and</li>
							<li>We may refuse to comply with your request for access or correction to your personal data and if we refuse to comply with such request, we will inform you of our refusal and reason for our refusal.</li>
						</ol>
					</li>
					<li>
						<p>Limit the process of your Personal Information<br>
						If you:</p>
						<ol type="a">
							<li>
								Do not wish to receive products and services, in particular about promotional offers, marketing information and event programs from us or third parties; or
							</li>
							<li>
								Would like to cease processing your Personal Information for as specified purpose or in a specified manner.
							</li>
						</ol>
					</li>					
				</ol>
				<div class="text-justify">
		  			<p>Please notify us in writing to the address set out in Section 11 above. In the absence of such written notification from you, we will deem it that you consent to the processing, usage and dissemination of your Personal Information</p>
					<p>in the manner, to the extent and within the scope of this notice. If you do not consent to the processing of your Personal Information as above we may not be able to provide you the products or other goods and services you requested.</p><br><br>
					<p>Date: 21 March 2014</p>
				</div>
			</div>
		</div>
	</div>
</div>
