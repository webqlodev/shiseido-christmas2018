@extends('layouts.email')
@section('content')
<table>
    <tr>
        <td style="text-align: center;">
            <img src="{{ asset('img/LauraMercier_logo.png') }}" alt="LauraMercier Logo" style="width:300px; margin-bottom: 50px;" />
        </td>
    </tr>
    <tr>
        <td>

            <p>Dear {{ $request->fullname  }},</p>

            <p>
                You may redeem this exclusive <strong>Deluxe Sample</strong> by presenting this verification code <strong>{{ $verificationCode }}</strong> at your preferred Laura Mercier store location at 
                <br><strong>{{ $request->location }}</strong>
            </p>

            <h4>Following is the info you provided to us:</h4>

            <p>
                <ul>
                    <li><strong>Your Name:</strong> {{ $request->fullname }}</li>
              		<li><strong>Email address:</strong> {{ $request->email }}</li>
                </ul>
            </p>

            <p>Thank you for your participation and see you soon!</p>

            <p>
                <a href="{{ route('redirect', ['target' => 'facebook']) }}" target="_blank">Follow Laura Mercier on Facebook</a>
            </p>

            <p style="margin-top: 20px; font-size: 10px;">
                <small>
                   Terms & Conditions: 
                    <br />
                    *Limited samples available. One redemption per customer.  Duplicate redemptions will not be entertained. Laura Mercier Malaysia reserves the right to modify or amend the validity of this offer without prior notice. Laura Mercier is available at Pavilion KL, 1Utama, Sunway Pyramid, Gurney Plaza, Aeon Tebrau City JB.
                </small>
            </p>
        </td>
    </tr>
</table>
@endsection