@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="lauramercier_desktop_logo" class="row pb-1 pt-0">
            <div class="col-xs-12">
                <img class="center-block" src="{{ asset('img/LauraMercier_logo.png') }}" width="150"/>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Oops! Page Not Found</h1>

                <p class="text-center">
                    We could not find the page you are looking for...
                </p>

                <h3 class="text-center">
                    <a href="{{ route('main') }}">Back to Home</a>
                </h3>
            </div>
        </div>
    </div>
@endsection
