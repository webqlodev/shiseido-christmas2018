@extends('layouts.app')
@section('content')
{{--<div id="floating-button" class="float-text upperfont floatbutton text-center animated bounce infinite">
    redeem now
</div>--}}
<div class="container-fluid pb-0" id="home">
    @include('layouts.header')
    <div class="col-md-12 gotham-bold-18pt pt-0 pb-1 text-center upperfont line-spacing">
            <div class="gotham-book-18pt text-spacing">city of lights</div>
            holiday collection 2018   
    </div> 
    @include('video')
    <div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-xs-12 gotham-book-13pt text-center text-justify pt-2 line-spacing">
        Laura Mercier Holiday 2018 Collection themed <span class="gotham-bold-13pt">City of Lights</span> captures the vivid colours and glamour of the holidays in the city imbued in an alluring festive glow.
    </div>
</div> <!-- container-fluid -->

    <div class="visible-lg-block visible-md-block">
        <div id="desktop-product" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.desktop-product')
            </div> <!-- container fluid -->
        </div>
    </div>

    <div class="visible-xs-block visible-sm-block">
        <div id="face" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.face-products')
            </div> <!-- container fluid -->
        </div>

        <div id="palettes" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.palettes-products')
            </div> <!-- container fluid -->
        </div>

        <div id="eyelips" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.eyelips-products')
            </div> <!-- container fluid -->
        </div>

        <div id="skincare" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.skincare-products')
            </div> <!-- container fluid -->
        </div>
        
        <div id="brush" class="pt-1 pb-1">
            <div class="container-fluid nopadding">
                @include('products.brush-products')
            </div> <!-- container fluid -->
        </div>
    </div>


     <div id="end_notice" class="container nopadding">
        <div class="row">
            <div class="col-xs-12 pt-0">
                <h1 class="text-center">
                    Thank you for your participation. 
                    <br>
                    Please stay tuned to our upcoming events.
                    <br>
                    <small>The redemption has ended on {{ Carbon\Carbon::parse( $end )->format('j F Y, ga') }}</small>
                </h1>
            </div>
        </div>
    </div>


@include('layouts.footer')
@endsection

@section('script')
<script>
$(function () {
    // remove error messages on input change
    $('input').change(function () {
        $(this).siblings('.help-block').empty().parents('.form-group').removeClass('has-error');
    });

    //auto scroll down event after clicked floating button
    $("#floating-button").click(function() {
    $('html,body').animate({
        scrollTop: $("#form").offset().top},
        1000);
    });

    //scroll to #form if there is any error
    $('window').ready(function () {
    if($('.has-error').length > 0 ){
        $('html,body').animate({
         scrollTop: $('#form').offset().top
        }, 1000);
    }
    });

    //carousel swipe event
    $(".carousel").swipe({
        swipe: function(
          event,
          direction,
          distance,
          duration,
          fingerCount,
          fingerData
        ) {
          if (direction == "left") $(this).carousel("next");
          if (direction == "right") $(this).carousel("prev");
        },
        allowPageScroll: "vertical"
    });
});
</script>
@endsection

    
